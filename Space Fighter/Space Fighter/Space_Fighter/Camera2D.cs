﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Camera2D
    {
        public Matrix viewMatrix;

        public Camera2D(ScreenManager sm)
        {
            ScreenManager = sm;
        }

        public ScreenManager ScreenManager
        {
            get;
            private set;
        }

        public Ship FocusedPlayer
        {
            get;
            set;
        }

        public Rectangle BackgroundRectangle
        {
            get;
            set;
        }

        public Vector2 Position
        {
            get;
            private set;
        }

        public void Update()
        {
            if (FocusedPlayer != null)
            {
                Position = new Vector2(
                     ScreenManager.Viewport.Width / 2 - FocusedPlayer.Position.X,
                     ScreenManager.Viewport.Height / 2 - FocusedPlayer.Position.Y);

                ClampCamera();
            }
            else
            {
                Position = Vector2.Zero;
            }

            viewMatrix = Matrix.CreateTranslation(Position.X, Position.Y, 0.0f);
        }

        public void ClampCamera()
        {
            if (Position.X > 0)
                Position = new Vector2(0, Position.Y);

            if (Position.X < ScreenManager.Viewport.Width - BackgroundRectangle.Width)
            {
                Position = new Vector2(ScreenManager.Viewport.Width - BackgroundRectangle.Width, Position.Y);
            }

            if (Position.Y > 0)
                Position = new Vector2(Position.X, 0);

            if (Position.Y < ScreenManager.Viewport.Height - BackgroundRectangle.Height)
            {
                Position = new Vector2(Position.X, ScreenManager.Viewport.Height - BackgroundRectangle.Height);
            }
        }

        public bool IsVisible(GameplayObject g)
        {
            Vector2 diff = g.Position + Position - new Vector2(ScreenManager.Viewport.Width / 2, ScreenManager.Viewport.Height / 2);
            if (diff.Length() < ScreenManager.Viewport.Width / 2 + g.Texture.Width)
            {
                return true;
            }
            else if (diff.Length() < ScreenManager.Viewport.Height / 2 + g.Texture.Height)
            {
                return true;
            }

            return false;
        }
    }
}
