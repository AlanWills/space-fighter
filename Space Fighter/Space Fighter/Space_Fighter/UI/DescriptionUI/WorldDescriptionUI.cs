﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class WorldDescriptionUI : UIBlock
    {
        public SpaceportData WorldData
        {
            get;
            private set;
        }

        public SpaceData RouteData
        {
            get;
            private set;
        }

        public SpriteFont Font
        {
            get;
            private set;
        }

        private Rectangle TextureRectangle
        {
            get;
            set;
        }

        public Image WorldImage
        {
            get;
            private set;
        }

        public Text WorldNameText
        {
            get;
            private set;
        }

        public Text TechLevelText
        {
            get;
            private set;
        }

        public Text EnemyNumberText
        {
            get;
            private set;
        }

        public Text AsteroidNumberText
        {
            get;
            private set;
        }

        public string WorldName
        {
            get;
            set;
        }

        public const int padding = 10;

        public WorldDescriptionUI(GraphicsDevice gd, Rectangle dims, Color c, SpriteFont font, string worldName)
            : base(gd, dims, c)
        {
            WorldName = worldName;
            Dimensions = new Rectangle((int)dims.X, (int)dims.Y, dims.Width, dims.Height);
            TextureRectangle = dims;
            Font = font;
        }

        public override void LoadContent(ContentManager content)
        {
            LoadBaseContent(content);

            base.LoadContent(content);
        }

        public void LoadBaseContent(ContentManager content)
        {
            if (WorldName != null)
            {
                WorldData = (SpaceportData)Game1.dataManager.GetMapData(String.Format("{0} Spaceport", WorldName));
                WorldData.LoadContent(content);

                RefreshUI(content);
            }
        }

        public void RefreshUI(ContentManager content)
        {
            WorldData = (SpaceportData)Game1.dataManager.GetMapData(String.Format("{0} Spaceport", WorldName));
            RouteData = (SpaceData)Game1.dataManager.GetMapData(WorldData.SpaceRouteName);
            WorldData.LoadContent(content);

            WorldImage = new Image(WorldData.ThumbnailTexture, new Vector2(Dimensions.Left, Dimensions.Top));
            WorldImage.Rectangle = TextureRectangle;

            WorldNameText = new Text(WorldData.WorldName, new Vector2(WorldImage.Rectangle.Left, WorldImage.Rectangle.Bottom + padding), Font);
            TechLevelText = new Text(WorldData.TechLevel.ToString(), new Vector2(WorldNameText.Position.X, WorldNameText.Position.Y + Font.LineSpacing + padding), Font);
            EnemyNumberText = new Text(String.Format("Enemies: {0}", RouteData.EnemyNumber), new Vector2(TechLevelText.Position.X, TechLevelText.Position.Y + Font.LineSpacing + padding), Font);
            AsteroidNumberText = new Text(String.Format("Asteroids: {0}", RouteData.AsteroidDensity.ToString()), new Vector2(EnemyNumberText.Position.X, EnemyNumberText.Position.Y + Font.LineSpacing + padding), Font);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (WorldName != null)
            {
                WorldImage.Draw(spriteBatch);
                WorldNameText.Draw(spriteBatch);
                TechLevelText.Draw(spriteBatch);
                EnemyNumberText.Draw(spriteBatch);
                AsteroidNumberText.Draw(spriteBatch);
            }
            
            base.Draw(spriteBatch);
        }

        public override void Update(Vector2 clickLocation)
        {
            base.Update(clickLocation);
        }
    }
}
