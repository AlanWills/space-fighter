﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class ShipDescriptionUI : UIBlock
    {
        public string ShipName
        {
            get;
            private set;
        }

        public ShipData ShipData
        {
            get;
            private set;
        }

        public SpriteFont Font
        {
            get;
            private set;
        }

        private Rectangle TextureRectangle
        {
            get;
            set;
        }

        public Image ShipImage
        {
            get;
            set;
        }

        public List<Image> ShipTurrets
        {
            get;
            set;
        }

        public Text ShipText
        {
            get;
            set;
        }

        public Text MaxHealthText
        {
            get;
            set;
        }

        public Text ShieldsText
        {
            get;
            set;
        }

        private const int padding = 10;

        public ShipDescriptionUI(GraphicsDevice gd, Rectangle dims, Color c, SpriteFont font, string shipName)
            : base(gd, dims, c)
        {
            ShipName = shipName;
            ShipData = Game1.dataManager.GetShipData(shipName);
            Dimensions = new Rectangle((int)dims.X, (int)dims.Y, dims.Width, dims.Height + 2 * font.LineSpacing + 3 * 10);
            TextureRectangle = dims;
            Font = font;

            ShipTurrets = new List<Image>();
        }

        public override void LoadContent(ContentManager content)
        {
            LoadBaseContent(content);

            base.LoadContent(content);
        }

        public void LoadBaseContent(ContentManager content)
        {
            ShipData.LoadContent(content);
            ShipImage = new Image(ShipData.Texture, new Vector2(Dimensions.Left, Dimensions.Top));
            ShipImage.Rectangle = TextureRectangle;

            ShipText = new Text(ShipName, new Vector2(ShipImage.Rectangle.Left, ShipImage.Rectangle.Bottom + padding), Font);

            MaxHealthText = new Text(
                String.Format("Max Health: {0}", ShipData.Health.ToString()),
                new Vector2(ShipText.Position.X, ShipText.Position.Y + Font.LineSpacing + padding),
                Font);

            string text = ShipData.ShieldsName != "" ? String.Format("Shields: {0}", ShipData.ShieldsName) : "No Shields";
            ShieldsText = new Text(
                text,
                new Vector2(MaxHealthText.Position.X, MaxHealthText.Position.Y + Font.LineSpacing + padding),
                Font);
        }

        public void SetUpTurretInfoUI(List<string> turretNames, Vector2 position)
        {
            ShipTurrets.Clear();
            foreach (string turret in turretNames)
            {
                TurretData t = Game1.dataManager.GetTurretData(turret);
                Image turretImage = new Image(
                                        t.Texture,
                                        new Vector2(position.X, position.Y + padding));

                position += new Vector2(0, t.Texture.Height);
                ShipTurrets.Add(turretImage);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            ShipImage.Draw(spriteBatch);
            ShipText.Draw(spriteBatch);
            MaxHealthText.Draw(spriteBatch);
            ShieldsText.Draw(spriteBatch);

            foreach(Image image in ShipTurrets)
            {
                image.Draw(spriteBatch);
            }
        }

        public override void Update(Vector2 clickLocation)
        {
            base.Update(clickLocation);
        }
    }
}
