﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class TurretDescriptionUI : UIBlock
    {
        public string TurretName
        {
            get;
            private set;
        }

        public TurretData TurretData
        {
            get;
            private set;
        }

        public SpriteFont Font
        {
            get;
            private set;
        }

        private Rectangle TextureRectangle
        {
            get;
            set;
        }

        private const int padding = 10;

        public TurretDescriptionUI(GraphicsDevice gd, Rectangle dims, Color c, SpriteFont font, string turretName)
            : base(gd, dims, c)
        {
            TurretName = turretName;
            TurretData = Game1.dataManager.GetTurretData(turretName);
            Dimensions = new Rectangle((int)dims.X, (int)dims.Y, dims.Width, dims.Height + 2 * font.LineSpacing + 3 * 10);
            TextureRectangle = dims;
            Font = font;
        }

        public override void LoadContent(ContentManager content)
        {
            TurretData.LoadContent(content);
            Image turretTexture = new Image(TurretData.Texture, new Vector2(Dimensions.Left, Dimensions.Top));
            turretTexture.Rectangle = TextureRectangle;
            Images.Add("TurretTexture", turretTexture);

            Text turretNameText = new Text(TurretName, new Vector2(turretTexture.Rectangle.Left, turretTexture.Rectangle.Bottom + padding), Font);
            Text.Add("TurretName", turretNameText);

            Text fireSpeedText = new Text(String.Format("Fire Speed: {0}", 1 / TurretData.TurretTimer), 
                new Vector2(turretNameText.Position.X, turretNameText.Position.Y + Font.LineSpacing + padding), Font);
            Text.Add("TurretFireSpeed", fireSpeedText);

            Text turretDamageText = new Text(String.Format("Damage: {0}", TurretData.TurretDamage),
                new Vector2(fireSpeedText.Position.X, fireSpeedText.Position.Y + Font.LineSpacing + padding), Font);
            Text.Add("TurretDamageText", turretDamageText);

            base.LoadContent(content);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public override void Update(Vector2 clickLocation)
        {
            base.Update(clickLocation);
        }
    }
}
