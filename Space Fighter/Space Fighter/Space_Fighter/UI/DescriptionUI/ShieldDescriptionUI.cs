﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class ShieldDescriptionUI : UIBlock
    {
        public string ShieldName
        {
            get;
            private set;
        }

        public ShieldData ShieldData
        {
            get;
            private set;
        }

        public SpriteFont Font
        {
            get;
            private set;
        }

        private Rectangle TextureRectangle
        {
            get;
            set;
        }

        public Image ShieldImage
        {
            get;
            set;
        }

        public Text ShieldNameText
        {
            get;
            set;
        }

        public Text ShieldStrengthText
        {
            get;
            set;
        }

        public Text ShieldRechargeRateText
        {
            get;
            set;
        }

        private const int padding = 10;

        public ShieldDescriptionUI(GraphicsDevice gd, Rectangle dims, Color c, SpriteFont font, string shieldName)
            : base (gd, dims, c)
        {
            ShieldName = shieldName;
            ShieldData = Game1.dataManager.GetShieldData(shieldName);
            Dimensions = new Rectangle((int)dims.X, (int)dims.Y, dims.Width, dims.Height + 2 * font.LineSpacing + 3 * 10);
            TextureRectangle = dims;
            Font = font;
        }

        public override void LoadContent(ContentManager content)
        {
            LoadBaseContent(content);

            base.LoadContent(content);
        }

        private void LoadBaseContent(ContentManager content)
        {
            ShieldData.LoadContent(content);
            ShieldImage = new Image(ShieldData.Texture, new Vector2(Dimensions.Left, Dimensions.Top));
            ShieldImage.Rectangle = TextureRectangle;

            ShieldNameText = new Text(ShieldName, new Vector2(ShieldImage.Rectangle.Left, ShieldImage.Rectangle.Bottom + padding), Font);
            ShieldStrengthText = new Text(
                String.Format("Strength = {0}", ShieldData.Health.ToString()),
                new Vector2(ShieldNameText.Position.X, ShieldNameText.Position.Y + Font.LineSpacing + padding),
                Font);

            ShieldRechargeRateText = new Text(
                String.Format("Recharge Rate: {0}s", ShieldData.RechargeRate.TotalSeconds.ToString()),
                new Vector2(ShieldStrengthText.Position.X, ShieldStrengthText.Position.Y + Font.LineSpacing + padding),
                Font);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            ShieldImage.Draw(spriteBatch);
            ShieldNameText.Draw(spriteBatch);
            ShieldStrengthText.Draw(spriteBatch);
            ShieldRechargeRateText.Draw(spriteBatch);
        }
    }
}
