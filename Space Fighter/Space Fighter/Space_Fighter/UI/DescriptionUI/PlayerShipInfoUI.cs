﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class PlayerShipInfoUI
    {
        public Player Player
        {
            get;
            private set;
        }

        public SpriteFont ScreenFont
        {
            get;
            private set;
        }

        public Rectangle Rectangle
        {
            get;
            private set;
        }

        public UIBlock PlayerShip
        {
            get;
            private set;
        }

        private const int padding = 10;

        #region Constructor

        public PlayerShipInfoUI(Player player, Rectangle r, GraphicsDevice gd, SpriteFont font)
        {
            Player = player;
            Rectangle = r;
            PlayerShip = new UIBlock(gd, r, Color.Red);
            ScreenFont = font;
        }

        #endregion

        #region Initialize()

        public void Initialize()
        {
            InitializePlayerShip();
        }

        private void InitializePlayerShip()
        {
            Image playerShip = new Image(
                                Player.PlayerShip.Texture, 
                                new Vector2(PlayerShip.Dimensions.Left + padding, PlayerShip.Dimensions.Top + padding));
            PlayerShip.Add("PlayerShip", playerShip);

            Text playerShipName = new Text(
                                    Player.PlayerShip.Name,
                                    new Vector2(playerShip.Rectangle.Right + padding, playerShip.Rectangle.Top),
                                    ScreenFont);
            PlayerShip.Add("PlayerShipName", playerShipName);

            Text playerShipHealth = new Text(
                                    String.Format("Health: {0}", Player.PlayerShip.Health.ToString()),
                                    new Vector2(playerShip.Rectangle.Left, playerShip.Rectangle.Bottom + padding),
                                    ScreenFont);
            PlayerShip.Add("PlayerShipHealth", playerShipHealth);

            Text playerTurret = new Text(
                                    String.Format("Turrets: {0}", Player.PlayerShip.FireType.ToString()),
                                    new Vector2(playerShipHealth.Position.X, playerShipHealth.Position.Y + ScreenFont.LineSpacing + padding),
                                    ScreenFont);
            PlayerShip.Add("TurretFireType", playerTurret);

            Text moneyText = new Text(
                                    String.Format("Money: {0}", Player.PlayerShip.ParentScreen.ScreenManager.Session.Money.ToString()),
                                    new Vector2(playerTurret.Position.X, playerTurret.Position.Y + ScreenFont.LineSpacing + padding),
                                    ScreenFont);
            PlayerShip.Add("PlayerMoney", moneyText);
        }

        #endregion

        public void Draw(SpriteBatch spriteBatch)
        {
            PlayerShip.Draw(spriteBatch);
        }

        public void Update(Vector2 clickLocation, GameTime gameTime)
        {
            PlayerShip.Update(clickLocation);
            if (PlayerShip.GetText("PlayerShipHealth").String != String.Format("Health: {0}", Player.PlayerShip.Health.ToString()))
                PlayerShip.GetText("PlayerShipHealth").String = String.Format("Health: {0}", Player.PlayerShip.Health.ToString());

            if (PlayerShip.GetText("TurretFireType").String != String.Format("Turrets: {0}", Player.PlayerShip.FireType.ToString()))
                PlayerShip.GetText("TurretFireType").String = String.Format("Turrets: {0}", Player.PlayerShip.FireType.ToString());

            if (PlayerShip.GetText("PlayerMoney").String != String.Format("Money: {0}", Player.PlayerShip.ParentScreen.ScreenManager.Session.Money.ToString()))
                PlayerShip.GetText("PlayerMoney").String = String.Format("Money: {0}", Player.PlayerShip.ParentScreen.ScreenManager.Session.Money.ToString());
        }
    }
}
