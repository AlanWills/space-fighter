﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Space_Fighter
{
    public class UIBlock
    {
        #region Properties and Fields

        Texture2D border, pixel;
        Rectangle borderTop, borderBottom, borderRight;

        public Rectangle Dimensions
        {
            get;
            set;
        }

        public Dictionary<string, Button> Buttons
        {
            get;
            private set;
        }

        public Dictionary<string, Text> Text
        {
            get;
            private set;
        }

        public Dictionary<string, Image> Images
        {
            get;
            private set;
        }

        public Dictionary<string, UIBlock> UIBlocks
        {
            get;
            private set;
        }

        Color colour;

        #endregion

        #region Constructors

        public UIBlock(GraphicsDevice gd, Texture2D borderTexture, Color borderColour, Rectangle dims)
        {
            border = borderTexture;
            pixel = new Texture2D(gd, 1, 1);
            Color[] c = new Color[1];
            c[0] = borderColour;
            pixel.SetData<Color>(c);

            Dimensions = dims;

            if (border != null)
            {
                borderTop = new Rectangle(Dimensions.Right - border.Width, Dimensions.Top, border.Width, border.Height);
                borderRight = new Rectangle(Dimensions.Right - 1, Dimensions.Top + border.Height, 1, Dimensions.Height - (border.Height * 2));
                borderBottom = new Rectangle(Dimensions.Right - border.Width, Dimensions.Bottom - border.Height, border.Width, border.Height);
            }

            Buttons = new Dictionary<string, Button>();
            Text = new Dictionary<string, Text>();
            Images = new Dictionary<string, Image>();
            UIBlocks = new Dictionary<string, UIBlock>();
        }

        public UIBlock(GraphicsDevice gd, Rectangle dims, Color c)
        {
            Dimensions = dims;
            colour = c;

            Buttons = new Dictionary<string, Button>();
            Text = new Dictionary<string, Text>();
            Images = new Dictionary<string, Image>();
            UIBlocks = new Dictionary<string, UIBlock>();
        }

        #endregion

        #region Methods

        public void Add(string s, Text t)
        {
            Text.Add(s, t);
        }

        public Text GetText(string s)
        {
            return Text[s];
        }

        public void Add(string s, Button b)
        {
            Buttons.Add(s, b);
        }

        public Button GetButton(string s)
        {
            if (Buttons.ContainsKey(s)) return Buttons[s];
            else return null;
        }

        public void Add(string s, Image i)
        {
            if (!Images.ContainsKey(s))
            {
                Images.Add(s, i);
            }
        }

        public Image GetImage(string s)
        {
            return Images[s];
        }

        public void Add(string s, UIBlock i)
        {
            UIBlocks.Add(s, i);
        }

        public UIBlock GetUIBlock(string s)
        {
            return UIBlocks[s];
        }

        public void ClearAll()
        {
            Text.Clear();
            Buttons.Clear();
            Images.Clear();
            UIBlocks.Clear();
        }

        public virtual void LoadContent(ContentManager content)
        {
            foreach (KeyValuePair<string, Button> b in Buttons)
            {
                b.Value.LoadContent(content);
            }

            foreach(KeyValuePair<string, UIBlock> ui in UIBlocks)
            {
                ui.Value.LoadContent(content);
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (border != null)
            {
                spriteBatch.Draw(border, borderTop, Color.White);
                spriteBatch.Draw(pixel, borderRight, Color.White);
                spriteBatch.Draw(border, borderBottom, Color.White);
            }

            foreach (var ui in UIBlocks)
            {
                ui.Value.Draw(spriteBatch);
            }

            foreach (var bentry in Buttons)
            {
                bentry.Value.Draw(spriteBatch);
            }

            foreach (var ientry in Images)
            {
                ientry.Value.Draw(spriteBatch);
            }

            foreach (var tentry in Text)
            {
                tentry.Value.Draw(spriteBatch);
            }
        }

        public virtual void Update(Vector2 clickLocation)
        {
            foreach (var ui in UIBlocks)
            {
                ui.Value.Update(clickLocation);
            }

            foreach (var bentry in Buttons)
            {
                bentry.Value.Update(clickLocation);
            }
        }

        #endregion
    }
}
