using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Space_Fighter
{
    public class Text
    {
        public string String
        {
            get;
            set;
        }

        public Vector2 Position
        {
            get;
            private set;
        }

        public SpriteFont Font
        {
            get;
            private set;
        }

        public object StoredObject
        {
            get;
            set;
        }

        public Text(string t, Vector2 p, SpriteFont f)
        {
            String = t;
            Position = p;
            Font = f;
        }

        public event EventHandler Update;

        public virtual void TextChanged()
        {
            if (Update != null)
            {
                Update(this, EventArgs.Empty);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(Font, String, Position, Color.Yellow);
        }
    }
}
