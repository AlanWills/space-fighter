using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Space_Fighter
{
    public class Button
    {
        public Text Text
        {
            get;
            private set;
        }

        public Vector2 Position
        {
            get;
            private set;
        }

        public Vector2 Centre
        {
            get 
            { 
                if (DrawnTexture != null)
                    return new Vector2(DrawnTexture.Width / 2, DrawnTexture.Height / 2);
                else
                {
                    return Vector2.Zero;
                }
            }
        }

        public float Rotation
        {
            get;
            private set;
        }

        public Rectangle Bounds
        {
            get;
            set;
        }

        public SpriteFont Font
        {
            get;
            private set;
        }

        public Texture2D DrawnTexture
        {
            get;
            private set;
        }

        public Texture2D NormalTexture
        {
            get;
            private set;
        }

        public string NormalTextureAsset
        {
            get;
            private set;
        }

        public Texture2D HighlightedTexture
        {
            get;
            private set;
        }

        public string HighlightedTextureAsset
        {
            get;
            private set;
        }

        private TimeSpan ClickTimeSpan
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }

        public object StoredObject
        {
            get;
            set;
        }

        public event EventHandler ClickEvent;

        public virtual void OnClick()
        {
            if (ClickEvent != null)
            {
                ClickEvent(this, EventArgs.Empty);
            }
        }

        public Button(string t, Vector2 p, string nta, string hta, SpriteFont f)
        {
            Text = new Text(t, new Vector2(p.X - f.MeasureString(t).X / 2, p.Y - f.LineSpacing / 2), f);
            Position = p;
            NormalTextureAsset = nta;
            HighlightedTextureAsset = hta;
            Font = f;
            IsActive = true;
        }

        public Button(string t, Vector2 p, float r, string nta, string hta, SpriteFont f)
        {
            Text = new Text(t, new Vector2(p.X - f.MeasureString(t).X / 2, p.Y - f.LineSpacing / 2), f);
            Position = p;
            Rotation = r;
            NormalTextureAsset = nta;
            HighlightedTextureAsset = hta;
            Font = f;
            IsActive = true;
        }

        public void LoadContent(ContentManager content)
        {
            NormalTexture = content.Load<Texture2D>(NormalTextureAsset);
            HighlightedTexture = content.Load<Texture2D>(HighlightedTextureAsset);

            DrawnTexture = NormalTexture;
            Bounds = new Rectangle(
                (int)Position.X - DrawnTexture.Width / 2, (int)Position.Y - DrawnTexture.Height / 2, 
                DrawnTexture.Width, DrawnTexture.Height);
        }

        public void Update(Vector2 location)
        {
            if (IsActive)
            {
                if (MathUtils.PointInRect(location, Bounds) && Bounds.Width != 0 && Bounds.Height != 0)
                {
                    DrawnTexture = HighlightedTexture;
                    OnClick();
                }

                // UpdateTexture(gameTime);
            }
        }

        public void UpdateTexture(GameTime gameTime)
        {
            ClickTimeSpan = ClickTimeSpan.Subtract(gameTime.ElapsedGameTime);

            if (ClickTimeSpan < TimeSpan.FromSeconds(0))
            {
                DrawnTexture = NormalTexture;
                ClickTimeSpan = TimeSpan.FromSeconds(0.1);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (IsActive)
            {
                /*spriteBatch.Draw(
                   DrawnTexture,
                   new Vector2(Position.X, Position.Y),
                   null,
                   Color.White,
                   Rotation,
                   Centre,
                   1f,
                   SpriteEffects.None,
                   0);*/

                spriteBatch.Draw(
                    DrawnTexture,
                    Bounds,
                    Color.White);

                Text.Draw(spriteBatch);
            }
        }
    }
}
