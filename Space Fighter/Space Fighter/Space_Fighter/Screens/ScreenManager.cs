using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Space_Fighter
{
    public class ScreenManager
    {
        public Session Session
        {
            get;
            private set;
        }

        public List<Screen> Screens
        {
            get;
            set;
        }

        public List<Screen> ScreensToRemove
        {
            get;
            set;
        }

        public List<Screen> ScreensToAdd
        {
            get;
            set;
        }

        public Screen CurrentScreen
        {
            get;
            set;
        }

        public GraphicsDevice GraphicsDevice
        {
            get;
            private set;
        }

        public Viewport Viewport
        {
            get;
            set;
        }

        public InputManager InputManager
        {
            get;
            set;
        }

        public ContentManager Content
        {
            get;
            set;
        }

        public Camera2D Camera
        {
            get;
            private set;
        }

        public ScreenManager(GraphicsDevice gd, ContentManager c, Game game)
        {
            Screens = new List<Screen>();
            ScreensToAdd = new List<Screen>();
            ScreensToRemove = new List<Screen>();
            InputManager = new InputManager(this);
            Session = new Session(Game1.dataManager);

            GraphicsDevice = gd;
            Camera = new Camera2D(this);
            Viewport = GraphicsDevice.Viewport;
            Content = c;
        }

        public void AddScreen(Screen s)
        {
            CurrentScreen = s;
            s.LoadScreenContent(Content);
        }

        public void RemoveScreen(Screen s)
        {
            // s.UnloadContent();
            // CurrentScreen = null;
        }

        public void DrawScreens(SpriteBatch spriteBatch)
        {
            CurrentScreen.Draw(spriteBatch);
        }

        public void DrawUI(SpriteBatch spriteBatch)
        {
            CurrentScreen.DrawUI(spriteBatch);
        }

        public void Update(GameTime gameTime)
        {
            InputManager.Update();
            CurrentScreen.Update(gameTime);

            Camera.Update();
        }
    }
}
