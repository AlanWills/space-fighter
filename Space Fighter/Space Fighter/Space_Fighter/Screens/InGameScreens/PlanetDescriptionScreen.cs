﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class PlanetDescriptionScreen : Screen
    {
        public SpaceportData PlanetData
        {
            get;
            private set;
        }

        public List<string> PlanetDescriptionText
        {
            get;
            private set;
        }

        public int SecondsToShow
        {
            get;
            private set;
        }

        public TimeSpan TimeSpan
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public PlanetDescriptionScreen(ScreenManager sm, DataManager dm, string name, string planet)
            : base(sm, dm, name)
        {
            PlanetData = dm.GetSpaceportData(planet);
            PlanetDescriptionText = new List<string>();
            PlanetDescriptionText = PlanetData.DescriptionText;
            SecondsToShow = 3;
            TimeSpan = TimeSpan.FromSeconds(SecondsToShow);
            Name = planet;
        }

        public override void LoadScreenContent(ContentManager content)
        {
            Font = content.Load<SpriteFont>(FontAsset);

            SetUpKeys();
        }

        public override void SetUpKeys()
        {
            base.SetUpKeys();

            KeyboardKeyInput skipDescription = new KeyboardKeyInput(true);
            skipDescription.Keys.Add(Keys.Escape);
            skipDescription.WhenKeyDownEvent += new EventHandler(skipDescription_Event);

            ScreenManager.InputManager.AddKey("Skip", skipDescription);
        }

        private void skipDescription_Event(object sender, EventArgs e)
        {
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new SpaceportScreen(ScreenManager, DataManager, String.Format("{0} Spaceport", Name)));
        }

        public override void Update(GameTime gameTime)
        {
            if (PlanetDescriptionText.Count > 0)
            {
                if (TimeSpan > TimeSpan.FromSeconds(0))
                {
                    TimeSpan = TimeSpan - gameTime.ElapsedGameTime;
                }
            }
            else
            {
                ScreenManager.RemoveScreen(this);
                ScreenManager.AddScreen(new SpaceportScreen(ScreenManager, DataManager, String.Format("{0} Spaceport", Name)));
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            if (PlanetDescriptionText.Count > 0)
            {
                if (TimeSpan > TimeSpan.FromSeconds(0))
                {
                    spriteBatch.DrawString(
                        Font,
                        PlanetDescriptionText[0],
                        new Vector2(
                            (BackgroundRectangle.Width - Font.MeasureString(PlanetDescriptionText[0]).X) / 2,
                            BackgroundRectangle.Height / 2 - Font.LineSpacing),
                        Color.White);
                }
                else
                {
                    TimeSpan = TimeSpan.FromSeconds(SecondsToShow);
                    PlanetDescriptionText.RemoveAt(0);
                }

                spriteBatch.DrawString(
                    Font, 
                    "Press Esc to Skip", 
                    new Vector2(
                            (BackgroundRectangle.Width - Font.MeasureString("Press Esc to Skip").X) / 2,
                            BackgroundRectangle.Height - Font.LineSpacing),
                    Color.Yellow);
            }
        }
    }
}
