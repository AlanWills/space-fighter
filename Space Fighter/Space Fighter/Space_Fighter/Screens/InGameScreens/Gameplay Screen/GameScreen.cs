using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Space_Fighter
{
    public class GameScreen : Screen
    {
        #region Fields

        public List<Enemy> Enemies
        {
            get;
            private set;
        }

        public List<Enemy> EnemiesToRemove
        {
            get;
            private set;
        }

        public Player Player
        {
            get;
            private set;
        }

        public ParticleSystem ParticleSystem
        {
            get;
            private set;
        }

        public ShipDescriptionUI ShipUI
        {
            get;
            private set;
        }

        public int AsteroidNumber
        {
            get;
            set;
        }

        public Vector2 BeaconLocation
        {
            get;
            set;
        }

        public string BeaconTextureAsset
        {
            get;
            set;
        }

        public Beacon Beacon
        {
            get;
            private set;
        }

        public List<string> EnemyNames
        {
            get;
            set;
        }

        Random r;

        #endregion

        #region Constructor

        public GameScreen(ScreenManager sm, DataManager dataManager, string name)
            : base(sm, dataManager, name)
        {
            r = new Random();

            Player = sm.Session.Player;
            Player.PlayerShip.Velocity = 0;
            Player.PlayerShip.ParentScreen = this;
            foreach (Turret t in Player.PlayerShip.Turrets)
            {
                t.ParentScreen = this;
            }

            SetUpKeys();
            SetUpEnemies();
            SetUpAsteroidParticleSystem();
            SetUpBeacon();
        }

        #endregion

        #region Set Up

        private void SetUpEnemies()
        {
            Enemies = new List<Enemy>();
            EnemiesToRemove = new List<Enemy>();

            foreach (string s in EnemyNames)
            {
                Enemies.Add(new Enemy(Vector2.Zero, this, s));
            }
        }

        public override void SetUpKeys()
        {
            base.SetUpKeys();

            KeyboardKeyInput moveForward = new KeyboardKeyInput(true);
            moveForward.Keys.Add(Keys.W);
            moveForward.WhenKeyDownEvent += new EventHandler(moveForward_Event);

            KeyboardKeyInput moveBackward = new KeyboardKeyInput(true);
            moveBackward.Keys.Add(Keys.S);
            moveBackward.WhenKeyDownEvent += new EventHandler(moveBackward_Event);

            KeyboardKeyInput rotateLeft = new KeyboardKeyInput(true);
            rotateLeft.Keys.Add(Keys.A);
            rotateLeft.WhenKeyDownEvent += new EventHandler(rotateLeft_Event);

            KeyboardKeyInput rotateRight = new KeyboardKeyInput(true);
            rotateRight.Keys.Add(Keys.D);
            rotateRight.WhenKeyDownEvent += new EventHandler(rotateRight_Event);

            KeyboardKeyInput rotateTurretsLeft = new KeyboardKeyInput(true);
            rotateTurretsLeft.Keys.Add(Keys.Left);
            rotateTurretsLeft.WhenKeyDownEvent += new EventHandler(rotateTurretsLeft_Event);

            KeyboardKeyInput rotateTurretsRight = new KeyboardKeyInput(true);
            rotateTurretsRight.Keys.Add(Keys.Right);
            rotateTurretsRight.WhenKeyDownEvent += new EventHandler(rotateTurretsRight_Event);

            KeyboardKeyInput fireWeapons = new KeyboardKeyInput(true);
            fireWeapons.Keys.Add(Keys.Space);
            fireWeapons.WhenKeyDownEvent += new EventHandler(fireWeapons_Event);

            KeyboardKeyInput changeFireType = new KeyboardKeyInput(false);
            changeFireType.Keys.Add(Keys.F);
            changeFireType.WhenKeyDownEvent += new EventHandler(changeFireType_Event);

            KeyboardKeyInput interactWithEnvironment = new KeyboardKeyInput(false);
            interactWithEnvironment.Keys.Add(Keys.R);
            interactWithEnvironment.WhenKeyDownEvent += new EventHandler(interact_Event);

            ScreenManager.InputManager.AddKey("MoveForward", moveForward);
            ScreenManager.InputManager.AddKey("MoveBackward", moveBackward);
            ScreenManager.InputManager.AddKey("RotateLeft", rotateLeft);
            ScreenManager.InputManager.AddKey("RotateRight", rotateRight);
            ScreenManager.InputManager.AddKey("RotateTurretsLeft", rotateTurretsLeft);
            ScreenManager.InputManager.AddKey("RotateTurretsRight", rotateTurretsRight);
            ScreenManager.InputManager.AddKey("FireWeapons", fireWeapons);
            ScreenManager.InputManager.AddKey("ChangeFireType", changeFireType);
            ScreenManager.InputManager.AddKey("Interact", interactWithEnvironment);
        }

        private void SetUpAsteroidParticleSystem()
        {
            int x, y;
            ParticleSystem = new ParticleSystem(ScreenManager);

            for (int i = 0; i < AsteroidNumber; i++)
            {
                x = r.Next(0, ScreenManager.CurrentScreen.BackgroundRectangle.Width);
                y = r.Next(0, ScreenManager.CurrentScreen.BackgroundRectangle.Height);
                ParticleSystem.Add(new Asteroid(new Vector2(x, y), this, "Textures\\Sprites\\Map Elements\\Asteroid", "Textures/Sprites/Map Elements/platinum_ore",
                    (float)r.NextDouble(), (float)r.NextDouble(), (float)r.NextDouble() / 30, (float)r.NextDouble()));
            }
        }

        private void SetUpBeacon()
        {
            Beacon = new Beacon(BeaconLocation, this, BeaconTextureAsset, this, ScreenManager);
        }

        #endregion

        #region Movement Events

        private void moveForward_Event(object sender, EventArgs e)
        {
            Player.PlayerShip.Acceleration -= 0.03f;
        }

        private void moveBackward_Event(object sender, EventArgs e)
        {
            Player.PlayerShip.Acceleration += 0.03f;
        }

        #endregion

        #region Rotation Events

        private void rotateLeft_Event(object sender, EventArgs e)
        {
            Player.PlayerShip.Rotation -= 0.05f;
        }

        private void rotateRight_Event(object sender, EventArgs e)
        {
            Player.PlayerShip.Rotation += 0.05f;
        }

        #endregion

        #region Fire Events

        private void fireWeapons_Event(object sender, EventArgs e)
        {
            foreach (Turret t in Player.PlayerShip.Turrets)
            {
                if (t.TurretTimer <= 0)
                {
                    t.FireBullet();
                }
            }
        }

        private void changeFireType_Event(object sender, EventArgs e)
        {
            if (Player.PlayerShip.AutoFireTurrets)
            {
                if (Player.PlayerShip.FireType == TurretFireType.Auto)
                {
                    Player.PlayerShip.FireType = TurretFireType.Manual;
                }
                else
                {
                    Player.PlayerShip.FireType = TurretFireType.Auto;
                }
            }
        }

        #endregion

        #region Turret Manual Rotation Events

        private void rotateTurretsLeft_Event(object sender, EventArgs e)
        {
            foreach (Turret t in Player.PlayerShip.Turrets)
            {
                t.TargetRotationOffset -= 0.09f;
            }
        }

        private void rotateTurretsRight_Event(object sender, EventArgs e)
        {
            foreach (Turret t in Player.PlayerShip.Turrets)
            {
                t.TargetRotationOffset += 0.09f;
            }
        }

        #endregion

        #region Interaction Events

        private void interact_Event(object sender, EventArgs e)
        {
            Beacon.CheckBeaconInteraction(Player);
        }

        #endregion

        #region UI

        private void AddStaticUI(ContentManager content)
        {
            string text = "";
            Vector2 textPos = new Vector2((ScreenManager.Viewport.Width - Font.MeasureString(text).X) / 2, Font.LineSpacing);
            Title = new Text(text, textPos, Font);

            ShipUI = new ShipDescriptionUI(
                ScreenManager.GraphicsDevice,
                new Rectangle(ScreenManager.Viewport.Width - 200, 20, 120, 150),
                Color.White,
                Font,
                Player.PlayerShip.Name);
            ShipUI.LoadContent(content);
        }

        #endregion

        #region Load Content

        public override void LoadScreenContent(ContentManager content)
        {
            Font = content.Load<SpriteFont>(FontAsset);

            Player.PlayerShip.LoadContent(content);
            AddStaticUI(content);
            LoadEnemyContent(content);

            Player.PlayerShip.Position = new Vector2(BackgroundRectangle.Width / 2, BackgroundRectangle.Height / 2);

            SetUpEnemies(content);
            ParticleSystem.LoadContent(content);
            Beacon.Font = Font;
            Beacon.LoadContent(content);

            base.LoadScreenContent(content);
        }

        private void LoadEnemyContent(ContentManager content)
        {
            foreach (Enemy e in Enemies)
            {
                e.LoadContent(content);
            }
        }

        private void SetUpEnemies(ContentManager content)
        {
            // Set up random positioning of enemies - or have preset spawn points.
            Random rand = new Random();
            Vector2 displacement = new Vector2((int)(BackgroundRectangle.Width / Enemies.Count), (int)(BackgroundRectangle.Height / Enemies.Count));

            foreach (Enemy e in Enemies)
            {
                int r = rand.Next(2 * Enemies.Count);
                int s = rand.Next(2 * Enemies.Count);
                e.Position = new Vector2(r * displacement.X, s * displacement.Y);
                e.SetUpTurrets(content);
            }
        }

        public override void UnloadContent()
        {
            ParticleSystem = null;
            Enemies = null;
            ShipUI = null;
            Beacon = null;

            ScreenManager.InputManager.KeyInputs.Clear();
            
            base.UnloadContent();
        }

        #endregion

        #region Update/Draw Methods

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            foreach (Enemy e in Enemies)
            {
                e.Draw(spriteBatch);
            }

            ParticleSystem.Draw(spriteBatch);
            Beacon.Draw(spriteBatch);
            Player.PlayerShip.Draw(spriteBatch);

            Vector2 diff = Player.PlayerShip.Position - Beacon.Position;
            if (diff.Length() < 200)
            {
                Beacon.DrawText(spriteBatch);
            }
        }

        public override void DrawUI(SpriteBatch spriteBatch)
        {
            ShipUI.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            ParticleSystem.Update(gameTime);

            foreach (Enemy e in EnemiesToRemove)
            {
                Enemies.Remove(e);
            }

            foreach (Enemy e in Enemies)
            {
                e.Update(gameTime);
            }

            Player.PlayerShip.Update(gameTime);
            ScreenManager.Camera.FocusedPlayer = Player.PlayerShip;
            Beacon.Update(gameTime);
            ShipUI.MaxHealthText.String = String.Format("Hull: {0}", Player.PlayerShip.Health.ToString());
            ShipUI.ShieldsText.String = String.Format("Shields: {0}", Player.PlayerShip.Shields.Health.ToString());

            base.Update(gameTime);
        }

        #endregion
    }
}
