﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class QuestScreen : GameScreen
    {
        public string QuestDescription
        {
            get;
            set;
        }

        public int RewardMoney
        {
            get;
            set;
        }

        public List<string> RewardCommodities
        {
            get;
            set;
        }

        public List<string> RewardTurrets
        {
            get;
            set;
        }

        public List<string> RewardShields
        {
            get;
            set;
        }

        public QuestScreen(ScreenManager screenManager, DataManager dataManager, string name)
            : base(screenManager, dataManager, name)
        {

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Enemies.Find(x => x.Name == EnemyNames[0]) == null)
            {
                GainRewards();
                ScreenManager.RemoveScreen(this);
                ScreenManager.AddScreen(new TravelToWorldScreen(ScreenManager, DataManager, "TravelToWorldScreen"));
            }
        }

        private void GainRewards()
        {
            ScreenManager.Session.Money += RewardMoney;
            Ship PlayerShip = ScreenManager.Session.PlayerShip;
            
            foreach (string s in RewardCommodities)
            {
                Commodity c = new Commodity();
                DataManager.LoadDataIntoInstance(c, s);

                ScreenManager.Session.Cargo.Add(c);
            }

            foreach (string s in RewardTurrets)
            {
                Turret t = new Turret(PlayerShip.Position, PlayerShip.ParentScreen, s, PlayerShip, PlayerShip.TurretOffsets[0]);
                PlayerShip.TurretOffsets.RemoveAt(0);

                PlayerShip.Turrets.Add(t);
                PlayerShip.TurretNames.Add(s);
            }

            foreach (string s in RewardShields)
            {
                PlayerShip.ShieldsName = s;
            }
        }
    }
}
