﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class QuestDescriptionScreen : Screen
    {
        public List<Button> Quests
        {
            get;
            private set;
        }

        public List<string> QuestsAvailable
        {
            get;
            private set;
        }

        public List<QuestData> QuestsData
        {
            get;
            private set;
        }

        public Text QuestDescription
        {
            get;
            private set;
        }

        public Text RewardsDescription
        {
            get;
            private set;
        }

        public Button AcceptQuestButton
        {
            get;
            private set;
        }

        const int padding = 10;

        public QuestDescriptionScreen(ScreenManager screenManager, DataManager dataManager, string name, SpaceportData spaceportData)
            : base(screenManager, dataManager, name)
        {
            QuestsData = new List<QuestData>();
            QuestsAvailable = spaceportData.QuestsAvailable;

            foreach (string s in QuestsAvailable)
            {
                QuestsData.Add(dataManager.GetQuestData(s));
            }
        }

        public override void LoadScreenContent(ContentManager content)
        {
            Font = content.Load<SpriteFont>(FontAsset);
            InitializeStartingUI(content);

            base.LoadScreenContent(content);
        }

        public override void InitializeStartingUI(ContentManager content)
        {
            int i = 0;

            foreach (QuestData qd in QuestsData)
            {
                i++;
                Button questButton = new Button(
                                        qd.QuestName,
                                        new Vector2(300, 200 * i),
                                        "Textures\\UI\\ButtonRegular",
                                        "Textures\\UI\\ButtonPressed",
                                        Font);
                questButton.StoredObject = qd.QuestName;
                questButton.ClickEvent += new EventHandler(questButton_ClickEvent);
                questButton.LoadContent(content);

                Buttons.Add(questButton);
            }

            QuestDescription = new Text("", new Vector2(BackgroundRectangle.Width - 900, 50), Font);
            Texts.Add(QuestDescription);

            AcceptQuestButton = new Button(
                                    "Accept", 
                                    new Vector2(BackgroundRectangle.Width - 100, BackgroundRectangle.Height - 100), 
                                    "Textures\\UI\\ButtonRegular", 
                                    "Textures\\UI\\ButtonPressed", 
                                    Font);
            AcceptQuestButton.ClickEvent += AcceptQuestButton_ClickEvent;
            AcceptQuestButton.IsActive = false;
            AcceptQuestButton.LoadContent(content);

            Buttons.Add(AcceptQuestButton);
        }

        private void AcceptQuestButton_ClickEvent(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b != null)
            {
                string name = b.StoredObject as string;

                if (name != null)
                {
                    ScreenManager.RemoveScreen(this);
                    ScreenManager.AddScreen(new QuestScreen(ScreenManager, DataManager, name));
                }
            }
        }

        private void questButton_ClickEvent(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b != null)
            {
                string name = b.StoredObject as string;

                if (name != null)
                {
                    QuestData qd = QuestsData.Find(x => x.QuestName == name);

                    string commodities = "", turrets = "", shields = "";
                    
                    foreach(string s in qd.RewardCommodities)
                    {
                        commodities = String.Format("{0}, {1}", s, commodities);
                    }

                    foreach (string s in qd.RewardTurrets)
                    {
                        turrets = String.Format("{0}, {1}", s, turrets);
                    }

                    foreach (string s in qd.RewardShields)
                    {
                        shields = String.Format("{0}, {1}", s, shields);
                    }

                    QuestDescription.String = String.Format("{0}\n\nRewards:\n\nMoney          {1}\nCommodities    {2}\nTurrets        {3}\nShields        {4}", 
                                                qd.QuestDescription,
                                                qd.RewardMoney.ToString(),
                                                commodities,
                                                turrets,
                                                shields);

                    AcceptQuestButton.StoredObject = name;
                    AcceptQuestButton.IsActive = true;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
