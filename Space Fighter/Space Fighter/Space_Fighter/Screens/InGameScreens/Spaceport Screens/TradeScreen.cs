﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class TradeScreen : Screen
    {
        private struct CommodityEntry
        {
            public Image Thumbnail;
            public Text Name;
            public Text BuyPrice;
            public Text SellPrice;
            public Text AmountOwned;
            public Text AmountAvailable;
            public Button BuyButton;
            public Button SellButton;

            public void Draw(SpriteBatch spriteBatch)
            {
                Thumbnail.Draw(spriteBatch);
                Name.Draw(spriteBatch);
                BuyPrice.Draw(spriteBatch);
                SellPrice.Draw(spriteBatch);
                AmountOwned.Draw(spriteBatch);
                AmountAvailable.Draw(spriteBatch);
                BuyButton.Draw(spriteBatch);
                SellButton.Draw(spriteBatch);
            }
        }

        private List<CommodityEntry> Commodities
        {
            get;
            set;
        }

        private SpaceportData PlanetData
        {
            get;
            set;
        }

        public Text MoneyText
        {
            get;
            private set;
        }

        private const int padding = 10;

        public TradeScreen(ScreenManager sm, DataManager dataManager, string name)
            : base(sm, dataManager, name)
        {
            Commodities = new List<CommodityEntry>();
            PlanetData = dataManager.GetSpaceportData(name);
            SetUpKeys();
        }

        public override void SetUpKeys()
        {
            base.SetUpKeys();

            KeyboardKeyInput backToPreviousScreen = new KeyboardKeyInput(true);
            backToPreviousScreen.Keys.Add(Keys.Escape);
            backToPreviousScreen.WhenKeyDownEvent += new EventHandler(back_Event);

            ScreenManager.InputManager.AddKey("BackToPreviousScreen", backToPreviousScreen);
        }

        private void back_Event(object sender, EventArgs e)
        {
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new SpaceportScreen(ScreenManager, DataManager, Name));
        }

        public override void LoadScreenContent(ContentManager content)
        {
            Font = content.Load<SpriteFont>(FontAsset);
            InitializeStartingUI(content);

            base.LoadScreenContent(content);
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void InitializeStartingUI(ContentManager content)
        {
            MoneyText = new Text(
                String.Format("Money: {0}", ScreenManager.Session.Money.ToString()),
                new Vector2(BackgroundRectangle.Width - Font.MeasureString(String.Format("Money: {0}", ScreenManager.Session.Money.ToString())).X - 50, Font.LineSpacing + 30),
                Font);
            Texts.Add(MoneyText);

            Text name = new Text("Name", new Vector2(300, 50), Font);
            Texts.Add(name);

            Text buyPrice = new Text("Buy Price", new Vector2(500, 50), Font);
            Texts.Add(buyPrice);

            Text sellPrice = new Text("Sell Price", new Vector2(700, 50), Font);
            Texts.Add(sellPrice);

            Text amountOwned = new Text("Amount Owned", new Vector2(900, 50), Font);
            Texts.Add(amountOwned);

            Text amountAvailable = new Text("Amount Available", new Vector2(1100, 50), Font);
            Texts.Add(amountAvailable);

            int index = 1;

            foreach (var cd in DataManager.CommodityData)
            {
                CommodityData commodityData = cd.Value;
                index++;

                int buyprice, sellprice, amountowned = 0, amountavailable = 10;;

                if (commodityData.Legal)
                {
                    buyprice = commodityData.TargetTechLevel == PlanetData.TechLevel ? 2 * commodityData.Cost : commodityData.Cost;
                    sellprice = commodityData.TargetTechLevel == PlanetData.TechLevel ? 2 * commodityData.Cost : (int)(commodityData.Cost * 0.8f);
                }
                else
                {
                    buyprice = commodityData.Cost;
                    sellprice = commodityData.Cost * 2;
                }

                foreach (Commodity c in ScreenManager.Session.Cargo)
                {
                    if (c.Name == commodityData.Name)
                        amountowned++;
                }

                CommodityEntry commodity = new CommodityEntry();
                commodity.Thumbnail = new Image(commodityData.Thumbnail, new Rectangle(100, (100 + padding) * index - 50, 100, 100));
                commodity.Name = new Text(commodityData.Name, new Vector2(300, (100 + padding) * index), Font);
                commodity.BuyPrice = new Text(buyprice.ToString(), new Vector2(500, (100 + padding) * index), Font);
                commodity.BuyPrice.StoredObject = buyprice;

                commodity.SellPrice = new Text(sellprice.ToString(), new Vector2(700, (100 + padding) * index), Font);
                commodity.SellPrice.StoredObject = sellprice;

                commodity.AmountOwned = new Text(amountowned.ToString(), new Vector2(900, (100 + padding) * index), Font);
                commodity.AmountOwned.StoredObject = amountowned;

                commodity.AmountAvailable = new Text(amountavailable.ToString(), new Vector2(1100, (100 + padding) * index), Font);
                commodity.AmountAvailable.StoredObject = amountavailable;

                commodity.BuyButton = new Button(
                                            "Buy",
                                            new Vector2(ScreenManager.Viewport.Width - 450, (100 + padding) * index),
                                            "Textures\\UI\\ButtonRegular",
                                            "Textures\\UI\\ButtonPressed",
                                            Font);
                commodity.BuyButton.StoredObject = commodity.Name.String;
                commodity.BuyButton.ClickEvent += new EventHandler(buyCommodity_Event);

                commodity.SellButton = new Button(
                                            "Sell",
                                            new Vector2(ScreenManager.Viewport.Width - 250, (100 + padding) * index),
                                            "Textures\\UI\\ButtonRegular",
                                            "Textures\\UI\\ButtonPressed",
                                            Font);
                commodity.SellButton.StoredObject = commodity.Name.String;
                commodity.SellButton.ClickEvent += new EventHandler(sellCommodity_Event);

                commodity.BuyButton.LoadContent(content);
                commodity.SellButton.LoadContent(content);
                Commodities.Add(commodity);
            }
        }

        private void buyCommodity_Event(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b != null)
            {
                string name = b.StoredObject as string;

                if (name != null)
                {
                    CommodityEntry c = Commodities.Find(x => x.Name.String == name);
                    int amountavailable = (int)c.AmountAvailable.StoredObject;

                    if (amountavailable > 0)
                    {
                        amountavailable--;
                        c.AmountAvailable.String = amountavailable.ToString();
                        c.AmountAvailable.StoredObject = amountavailable;

                        ScreenManager.Session.BuyCargo(c.Name.String, (int)c.BuyPrice.StoredObject);

                        int amount = (int)c.AmountOwned.StoredObject;
                        amount++;
                        c.AmountOwned.String = amount.ToString();
                        c.AmountOwned.StoredObject = amount;
                    }
                }
            }
        }

        private void sellCommodity_Event(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b != null)
            {
                string name = b.StoredObject as string;

                if (name != null)
                {
                    CommodityEntry c = Commodities.Find(x => x.Name.String == name);
                    int amount = (int)c.AmountOwned.StoredObject;

                    if (amount > 0)
                    {
                        amount--;
                        c.AmountOwned.String = amount.ToString();
                        c.AmountOwned.StoredObject = amount;

                        ScreenManager.Session.SellCargo(name, (int)c.SellPrice.StoredObject);

                        int amountavailable = (int)c.AmountAvailable.StoredObject;
                        amountavailable++;
                        c.AmountAvailable.String = amountavailable.ToString();
                        c.AmountAvailable.StoredObject = amountavailable;
                    }
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            foreach (CommodityEntry commodity in Commodities)
            {
                commodity.Draw(spriteBatch);
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            foreach (CommodityEntry ce in Commodities)
            {
                ce.BuyButton.Update(ScreenManager.InputManager.Mouse.MouseClickedPosition);
                ce.SellButton.Update(ScreenManager.InputManager.Mouse.MouseClickedPosition);

                if (!ScreenManager.Session.HasCommodity(ce.Name.String))
                {
                    ce.SellButton.IsActive = false;
                }
                else
                {
                    ce.SellButton.IsActive = true;
                }

                if ((int)ce.AmountAvailable.StoredObject > 0 && (int)(ce.BuyPrice.StoredObject) <= ScreenManager.Session.Money)
                {
                    ce.BuyButton.IsActive = true;
                }
                else
                {
                    ce.BuyButton.IsActive = false;
                }
            }

            MoneyText.String = String.Format("Money: {0}", ScreenManager.Session.Money.ToString());
        }
    }
}