﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class SpaceportScreen : Screen
    {
        public SpaceportData SpaceportData
        {
            get;
            private set;
        }

        public string WorldName
        {
            get;
            set;
        }

        public TechnologyLevel TechLevel
        {
            get;
            set;
        }

        private const int padding = 20;

        public SpaceportScreen(ScreenManager screenManager, DataManager dataManager, string name)
            : base(screenManager, dataManager, name)
        {
            SpaceportData = (SpaceportData)DataManager.GetMapData(name);
        }

        public override void LoadScreenContent(ContentManager content)
        {
            Font = content.Load<SpriteFont>(FontAsset);
            InitializeStartingUI(content);

            base.LoadScreenContent(content);
        }

        public override void InitializeStartingUI(ContentManager content)
        {
            Vector2 textPos = new Vector2((ScreenManager.Viewport.Width - Font.MeasureString(Name).X) / 2, 4 * Font.LineSpacing);
            Title = new Text(Name, textPos, Font);

            Vector2 shipyardButtonPos = new Vector2(ScreenManager.Viewport.Width / 2, ScreenManager.Viewport.Height / 4 - 30);
            Button shipyardButton = new Button("Shipyard", shipyardButtonPos, "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            shipyardButton.ClickEvent += new EventHandler(shipyardButton_ClickEvent);
            Buttons.Add(shipyardButton);

            Vector2 tradeButtonPos = shipyardButtonPos + new Vector2(0, 100 + padding);
            Button tradeButton = new Button("Trade", tradeButtonPos, "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            tradeButton.ClickEvent += new EventHandler(tradeButton_ClickEvent);
            Buttons.Add(tradeButton);

            Vector2 questsButtonPos = tradeButtonPos + new Vector2(0, 100 + padding);
            Button questsButton = new Button("Quests", questsButtonPos, "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            questsButton.ClickEvent += new EventHandler(questsButton_ClickEvent);
            Buttons.Add(questsButton);

            Vector2 leaveButtonPos = questsButtonPos + new Vector2(0, 100 + padding);
            Button leaveButton = new Button("Leave", leaveButtonPos, "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            leaveButton.ClickEvent += new EventHandler(leaveButton_ClickEvent);
            Buttons.Add(leaveButton);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        #region Button Click Events

        private void shipyardButton_ClickEvent(object sender, EventArgs e)
        {
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new ShipyardScreen(ScreenManager, DataManager, Name, SpaceportData));
        }

        private void tradeButton_ClickEvent(object sender, EventArgs e)
        {
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new TradeScreen(ScreenManager, DataManager, Name));
        }

        private void questsButton_ClickEvent(object sender, EventArgs e)
        {
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new QuestDescriptionScreen(ScreenManager, DataManager, Name, SpaceportData));
        }

        private void leaveButton_ClickEvent(object sender, EventArgs e)
        {
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new TravelToWorldScreen(ScreenManager, DataManager, "TravelToWorldScreen"));
        }
        #endregion
    }
}
