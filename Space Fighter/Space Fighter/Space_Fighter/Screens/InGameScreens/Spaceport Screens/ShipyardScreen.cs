﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class ShipyardScreen : Screen
    {
        public SpaceportData SpaceportData
        {
            get;
            private set;
        }

        const int padding = 10;

        Text currentHealthText, autoFireEnabled, playerMoneyText;
        Button repairShipButton, buyShipButton, sellShipButton, buyWeaponsButton, buyShieldsButton, buyUpgradesButton;
        UIBlock buyShipUI, buyWeaponsUI, buyShieldsUI, currentUI;
        ShipDescriptionUI shipDescriptionUI;

        public ShipyardScreen(ScreenManager screenManager, DataManager dataManager, string name, SpaceportData spaceportData)
            : base(screenManager, dataManager, name)
        {
            SpaceportData = spaceportData;
            SetUpKeys();
        }

        public override void SetUpKeys()
        {
            base.SetUpKeys();

            KeyboardKeyInput backToPreviousScreen = new KeyboardKeyInput(true);
            backToPreviousScreen.Keys.Add(Keys.Escape);
            backToPreviousScreen.WhenKeyDownEvent += new EventHandler(back_Event);

            ScreenManager.InputManager.AddKey("BackToPreviousScreen", backToPreviousScreen);
        }

        public override void LoadScreenContent(ContentManager content)
        {
            Font = content.Load<SpriteFont>(FontAsset);
            InitializeStartingUI(content);
            InitializeShipsToBuyUI(content);
            InitializeWeaponsToBuy(content);
            InitializeShieldsToBuy(content);

            base.LoadScreenContent(content);
        }

        public override void UnloadContent()
        {
            currentHealthText = null;
            autoFireEnabled = null;
            repairShipButton = null;
            buyShipButton = null;
            sellShipButton = null;
            buyWeaponsButton = null;
            buyShieldsButton = null;
            buyUpgradesButton = null;
            buyShipUI = null;
            buyWeaponsUI = null;
            buyShieldsUI = null;
            
            base.UnloadContent();
        }

        public override void InitializeStartingUI(ContentManager content)
        {
            // Initialize the player ship info
            Player player = ScreenManager.Session.Player; 
            Vector2 currentShipPos = new Vector2(BackgroundRectangle.Width - 220, 80);
            UpdateShipDescriptionUI();

            int diff = DataManager.GetShipData(player.PlayerShip.Name).Health - player.PlayerShip.Health;
            string repairString = ScreenManager.Session.Money >= diff ? String.Format("Repair {0}", diff.ToString()) : String.Format("Repair {0}", ScreenManager.Session.Money.ToString());

            repairShipButton = new Button(repairString, new Vector2(currentShipPos.X - 120, 100), 
                "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            repairShipButton.ClickEvent += new EventHandler(repairShip_Event);
            repairShipButton.Text.Update += new EventHandler(updateRepairButtonText_Event);
            AddButton(repairShipButton);

            buyShipButton = new Button("Buy Ship", 
                new Vector2(repairShipButton.Position.X, repairShipButton.Position.Y + 100 + padding),
                "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            buyShipButton.ClickEvent += new EventHandler(showShipsToBuy_Event);
            AddButton(buyShipButton);

            sellShipButton = new Button("Sell Ship",
                new Vector2(buyShipButton.Position.X, buyShipButton.Position.Y + 100 + padding),
                "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            sellShipButton.ClickEvent += new EventHandler(sellShip_Event);
            AddButton(sellShipButton);

            buyWeaponsButton = new Button("Weapons",
                new Vector2(sellShipButton.Position.X, sellShipButton.Position.Y + 100 + padding),
                "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            buyWeaponsButton.ClickEvent += new EventHandler(showWeaponsToBuy_Event);
            AddButton(buyWeaponsButton);

            buyShieldsButton = new Button("Shields",
                new Vector2(buyWeaponsButton.Position.X, buyWeaponsButton.Position.Y + 100 + padding),
                "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            buyShieldsButton.ClickEvent += new EventHandler(showShieldsToBuy_Event);
            AddButton(buyShieldsButton);

            buyUpgradesButton = new Button("Upgrades",
                new Vector2(buyShieldsButton.Position.X, buyShieldsButton.Position.Y + 100 + padding),
                "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            AddButton(buyUpgradesButton);
        }

        private void UpdateShipDescriptionUI()
        {
            Player player = ScreenManager.Session.Player;

            Vector2 currentShipPos = new Vector2(BackgroundRectangle.Width - 220, 80);
            shipDescriptionUI = new ShipDescriptionUI(
                                                    ScreenManager.GraphicsDevice,
                                                    new Rectangle((int)currentShipPos.X, (int)currentShipPos.Y, 120, 150),
                                                    Color.White,
                                                    Font,
                                                    player.PlayerShip.Name);

            currentHealthText = new Text(
                String.Format("Current Health: {0}", player.PlayerShip.Health.ToString()),
                new Vector2(shipDescriptionUI.Dimensions.Left, shipDescriptionUI.Dimensions.Bottom + Font.LineSpacing + padding),
                Font);
            currentHealthText.Update += new EventHandler(updatePlayerHealth_Event);
            shipDescriptionUI.Text.Add("ShipCurrentHealth", currentHealthText);

            autoFireEnabled = new Text(
                String.Format("Auto Fire: {0}", player.PlayerShip.AutoFireTurrets.ToString()),
                new Vector2(currentHealthText.Position.X, currentHealthText.Position.Y + Font.LineSpacing + padding),
                Font);
            shipDescriptionUI.Text.Add("AutoFire", autoFireEnabled);

            playerMoneyText = new Text(
                String.Format("Money: {0}", ScreenManager.Session.Money),
                new Vector2(autoFireEnabled.Position.X, autoFireEnabled.Position.Y + Font.LineSpacing + padding),
                Font);
            playerMoneyText.Update += new EventHandler(updatePlayerMoney_Event);
            shipDescriptionUI.Text.Add("PlayerMoney", playerMoneyText);

            shipDescriptionUI.LoadBaseContent(ScreenManager.Content);
            shipDescriptionUI.SetUpTurretInfoUI(
                player.PlayerShip.TurretNames, 
                new Vector2(playerMoneyText.Position.X, playerMoneyText.Position.Y + Font.LineSpacing + padding));
        }

        #region Button ClickEvents

        private void back_Event(object sender, EventArgs e)
        {
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new SpaceportScreen(ScreenManager, DataManager, Name));
        }

        private void repairShip_Event(object sender, EventArgs e)
        {
            int diff = (DataManager.GetShipData(ScreenManager.Session.Player.PlayerShip.Name).Health - ScreenManager.Session.Player.PlayerShip.Health);

            if (ScreenManager.Session.Money > diff)
            {
                ScreenManager.Session.Money -= (DataManager.GetShipData(ScreenManager.Session.Player.PlayerShip.Name).Health - ScreenManager.Session.Player.PlayerShip.Health);
                ScreenManager.Session.Player.PlayerShip.Health += diff;
            }
            else
            {
                ScreenManager.Session.Money = 0;
                ScreenManager.Session.Player.PlayerShip.Health += ScreenManager.Session.Money;
            }
            
            repairShipButton.Text.TextChanged();
            currentHealthText.TextChanged();
            playerMoneyText.TextChanged();
            repairShipButton.IsActive = false;
        }

        private void showShipsToBuy_Event(object sender, EventArgs e)
        {
            currentUI = buyShipUI;
        }

        private void showWeaponsToBuy_Event(object sender, EventArgs e)
        {
            currentUI = buyWeaponsUI;
        }

        private void showShieldsToBuy_Event(object sender, EventArgs e)
        {
            currentUI = buyShieldsUI;
        }

        private void sellShip_Event(object sender, EventArgs e)
        {
            ScreenManager.Session.Money += ScreenManager.Session.Player.PlayerShip.Cost / 2;
            playerMoneyText.TextChanged();
            sellShipButton.IsActive = false;
            repairShipButton.IsActive = false;
        }

        #endregion

        #region Text Events

        private void updatePlayerMoney_Event(object sender, EventArgs e)
        {
            playerMoneyText.String = String.Format("Money: {0}", ScreenManager.Session.Money.ToString());
        }

        private void updatePlayerHealth_Event(object sender, EventArgs e)
        {
            currentHealthText.String = String.Format("Current Health: {0}", ScreenManager.Session.Player.PlayerShip.Health);
        }

        private void updateRepairButtonText_Event(object sender, EventArgs e)
        {
            repairShipButton.Text.String = "Repair 0";
        }

        #endregion

        #region UI Initializers

        private void InitializeShipsToBuyUI(ContentManager content)
        {
            buyShipUI = new UIBlock(ScreenManager.GraphicsDevice, new Rectangle(100, 200, 600, 400), Color.Cyan);
            Dictionary<string, ShipData> shipData = DataManager.ShipData;

            int index = 0;
            foreach (KeyValuePair<string, ShipData> s in shipData)
            {
                if (s.Value.TechLevel <= SpaceportData.TechLevel)
                {
                    ShipDescriptionUI shipUI = new ShipDescriptionUI(
                                                ScreenManager.GraphicsDevice, 
                                                new Rectangle(200 * (index + 1), 200, 90, 150), 
                                                Color.White, 
                                                Font, 
                                                s.Key);

                    Vector2 turretPos = new Vector2(shipUI.Dimensions.X, shipUI.Dimensions.Bottom + 2*padding);
                    shipUI.SetUpTurretInfoUI(s.Value.StartingTurretNames, turretPos);

                    Text shipCostText = new Text(
                        String.Format("Cost: {0}", s.Value.Cost),
                        new Vector2(shipUI.Dimensions.X, shipUI.Dimensions.Bottom + 3*Font.LineSpacing),
                        Font);
                    shipUI.Add("ShipCost", shipCostText);

                    Button buyShipButton = new Button("Buy", new Vector2(shipUI.Dimensions.Center.X + 10, shipCostText.Position.Y + 50 + padding),
                        "Textures/UI/ButtonRegular", "Textures/UI/ButtonPressed", Font);
                    buyShipButton.StoredObject = s.Key;
                    buyShipButton.ClickEvent += new EventHandler(buyShipLeftClick);
                    shipUI.Add("BuyShip", buyShipButton);

                    buyShipUI.Add(String.Format("Ship{0}", index), shipUI);
                    index++;
                }
            }

            buyShipUI.LoadContent(content);
        }

        private void buyShipLeftClick(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (b != null)
            {
                string shipKey = b.StoredObject as string;
                if (shipKey != null)
                {
                    ScreenManager.Session.PurchaseShip(shipKey);
                    UpdateShipDescriptionUI();
                }
            }
        }

        private void InitializeWeaponsToBuy(ContentManager content)
        {
            buyWeaponsUI = new UIBlock(ScreenManager.GraphicsDevice, new Rectangle(100, 200, 600, 400), Color.Cyan);
            Dictionary<string, TurretData> turretData = DataManager.TurretData;
            ShipData playerShipData = DataManager.GetShipData(ScreenManager.Session.PlayerShip.Name);

            int index = 0;
            foreach (KeyValuePair<string, TurretData> t in turretData)
            {
                if (t.Value.TechLevel <= SpaceportData.TechLevel && playerShipData.TechLevel >= t.Value.TechLevel)
                {
                    TurretDescriptionUI turretUI = new TurretDescriptionUI(
                                                ScreenManager.GraphicsDevice,
                                                new Rectangle(200 * (index + 1), 200, 30, 50),
                                                Color.White,
                                                Font,
                                                t.Key);

                    Text turretCostText = new Text(
                        String.Format("Cost: {0}", t.Value.Cost),
                        new Vector2(turretUI.Dimensions.X, turretUI.Dimensions.Bottom + 2 * Font.LineSpacing),
                        Font);
                    turretUI.Add("TurretCost", turretCostText);

                    Button buyWeaponButton = new Button("Buy", new Vector2(turretUI.Dimensions.Center.X + 40, turretCostText.Position.Y + 50 + padding),
                        "Textures/UI/ButtonRegular", "Textures/UI/ButtonPressed", Font);
                    buyWeaponButton.StoredObject = t.Key;
                    buyWeaponButton.ClickEvent += new EventHandler(buyWeaponsLeftClick);
                    turretUI.Add("BuyWeapon", buyWeaponButton);

                    buyWeaponsUI.Add(String.Format("Turret{0}", index), turretUI);
                    index++;
                }
            }

            buyWeaponsUI.LoadContent(content);
        }

        private void buyWeaponsLeftClick(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (b != null)
            {
                string turretKey = b.StoredObject as string;
                if (turretKey != null)
                {
                    ScreenManager.Session.PurchaseTurret(turretKey);
                    UpdateShipDescriptionUI();
                }
            }
        }

        private void InitializeShieldsToBuy(ContentManager content)
        {
            buyShieldsUI = new UIBlock(ScreenManager.GraphicsDevice, new Rectangle(100, 200, 600, 400), Color.Cyan);
            Dictionary<string, ShieldData> shieldData = DataManager.ShieldData;
            ShipData playerShipData = DataManager.GetShipData(ScreenManager.Session.PlayerShip.Name);

            int index = 0;
            foreach (KeyValuePair<string, ShieldData> s in shieldData)
            {
                if (s.Value.TechLevel <= SpaceportData.TechLevel && playerShipData.TechLevel >= s.Value.TechLevel)
                {
                    ShieldDescriptionUI shieldUI = new ShieldDescriptionUI(
                                                ScreenManager.GraphicsDevice,
                                                new Rectangle(250 * (index + 1), 200, 100, 100),
                                                Color.White,
                                                Font,
                                                s.Key);

                    Text shieldCostText = new Text(
                        String.Format("Cost: {0}", s.Value.Cost),
                        new Vector2(shieldUI.Dimensions.X, shieldUI.Dimensions.Bottom + 2 * Font.LineSpacing),
                        Font);
                    shieldUI.Add("ShieldCost", shieldCostText);

                    Button buyShieldButton = new Button("Buy", new Vector2(shieldUI.Dimensions.Center.X + 40, shieldCostText.Position.Y + 50 + padding),
                        "Textures/UI/ButtonRegular", "Textures/UI/ButtonPressed", Font);
                    buyShieldButton.StoredObject = s.Key;
                    buyShieldButton.ClickEvent += new EventHandler(buyShields_ClickEvent);
                    shieldUI.Add("BuyWeapon", buyShieldButton);

                    buyShieldsUI.Add(String.Format("Shield{0}", index), shieldUI);
                    index++;
                }
            }

            buyShieldsUI.LoadContent(content);
        }

        private void buyShields_ClickEvent(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (b != null)
            {
                string shieldsKey = b.StoredObject as string;
                if (shieldsKey != null)
                {
                    ScreenManager.Session.PurchaseShield(shieldsKey);
                    UpdateShipDescriptionUI();
                }
            }
        }

        #endregion

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            if (shipDescriptionUI != null)
                shipDescriptionUI.Draw(spriteBatch);

            if (currentUI != null)
                currentUI.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (currentUI != null)
                currentUI.Update(ScreenManager.InputManager.Mouse.MouseClickedPosition);
        }
    }
}
