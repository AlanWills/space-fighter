﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class TravelToWorldScreen : Screen
    {
        public List<SpaceportData> Worlds
        {
            get;
            private set;
        }

        public List<Button> WorldThumbnails
        {
            get;
            private set;
        }

        public WorldDescriptionUI ClickedWorld
        {
            get;
            private set;
        }

        public Button TravelButton
        {
            get;
            private set;
        }

        public TravelToWorldScreen(ScreenManager screenManager, DataManager dataManager, string name)
            : base(screenManager, dataManager, name)
        {
            Worlds = new List<SpaceportData>();
            WorldThumbnails = new List<Button>();

            foreach (var s in DataManager.WorldsData)
            {
                Worlds.Add(s.Value);
            }
        }

        public override void LoadScreenContent(ContentManager content)
        {
            Font = content.Load<SpriteFont>(FontAsset);
            InitializeStartingUI(content);

            ClickedWorld = new WorldDescriptionUI(
                ScreenManager.GraphicsDevice,
                new Rectangle(BackgroundRectangle.Width - 200, 30, 100, 100),
                Color.White,
                Font,
                ScreenManager.Session.CurrentSpaceport);
            ClickedWorld.LoadContent(content);

            base.LoadScreenContent(content);
        }

        public override void InitializeStartingUI(ContentManager content)
        {
            string text = "Travel To World";
            Vector2 textPos = new Vector2((ScreenManager.Viewport.Width - Font.MeasureString(text).X) / 2, Font.LineSpacing + 10);
            Title = new Text(text, textPos, Font);

            foreach (SpaceportData sd in Worlds)
            {
                Button thumbnail = new Button("", sd.MapPosition, sd.ThumbnailTextureAsset, sd.ThumbnailTextureAsset, Font);
                thumbnail.StoredObject = sd.WorldName;
                thumbnail.ClickEvent += new EventHandler(worldClicked_Event);
                thumbnail.LoadContent(content);
                WorldThumbnails.Add(thumbnail);
            }

            TravelButton = new Button(
                "Travel",
                new Vector2(ScreenManager.Viewport.Width - 200, ScreenManager.Viewport.Height - 100),
                "Textures\\UI\\ButtonRegular",
                "Textures\\UI\\ButtonPressed",
                Font);
            TravelButton.LoadContent(content);
            TravelButton.IsActive = false;
            TravelButton.ClickEvent += TravelButton_ClickEvent;
        }

        void TravelButton_ClickEvent(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b != null)
            {
                string name = b.StoredObject as string;

                if (name != null)
                {
                    ScreenManager.Session.DestinationSpaceport = name;
                }

                SpaceportData sd = DataManager.GetSpaceportData(name);

                if (sd != null)
                {
                    ScreenManager.RemoveScreen(this);
                    ScreenManager.AddScreen(new GameScreen(ScreenManager, DataManager, sd.SpaceRouteName));
                }
            }            
        }

        private void worldClicked_Event(object sender, EventArgs e)
        {
            Button b = sender as Button;

            if (b != null)
            {
                string name = b.StoredObject as string;

                if (name != null)
                {
                    if (name != ScreenManager.Session.CurrentSpaceport)
                    {
                        ClickedWorld.WorldName = name;
                        ClickedWorld.RefreshUI(ScreenManager.Content);
                        TravelButton.StoredObject = name;
                        TravelButton.IsActive = true;
                    }
                    else
                    {
                        ClickedWorld.WorldName = name;
                        ClickedWorld.RefreshUI(ScreenManager.Content);
                        TravelButton.IsActive = false;
                    }
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {   
            base.Draw(spriteBatch);

            foreach (Button i in WorldThumbnails)
            {
                i.Draw(spriteBatch);
            }

            ClickedWorld.Draw(spriteBatch);
            TravelButton.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            foreach (Button b in WorldThumbnails)
            {
                b.Update(ScreenManager.InputManager.Mouse.MouseClickedPosition);
            }

            TravelButton.Update(ScreenManager.InputManager.Mouse.MouseClickedPosition);

            base.Update(gameTime);
        }
    }
}
