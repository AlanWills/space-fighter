using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Space_Fighter
{
    public class Screen
    {   
        public Texture2D Background
        {
            get;
            set;
        }

        public SpriteFont Font
        {
            get;
            protected set;
        }

        public string FontAsset
        {
            get;
            set;
        }

        public Rectangle BackgroundRectangle
        {
            get;
            set;
        }

        public Text Title
        {
            get;
            set;
        }

        public List<Button> Buttons
        {
            get;
            set;
        }

        public List<Button> ButtonsToRemove
        {
            get;
            set;
        }

        public List<Image> Images
        {
            get;
            set;
        }

        public List<Text> Texts
        {
            get;
            set;
        }

        public List<UIBlock> UIBlocks
        {
            get;
            set;
        }

        public string Name
        {
            get;
            private set;
        }

        public ScreenManager ScreenManager;
        public DataManager DataManager;

        public Screen(ScreenManager screenManager, DataManager dataManager, string name)
        {
            Buttons = new List<Button>();
            ButtonsToRemove = new List<Button>();
            Images = new List<Image>();
            Texts = new List<Text>();
            UIBlocks = new List<UIBlock>();
            BackgroundRectangle = new Rectangle();
            screenManager.InputManager.KeyInputs = new Dictionary<string, KeyboardKeyInput>();

            Name = name;
            ScreenManager = screenManager;
            DataManager = dataManager;
            ScreenManager.CurrentScreen = this;

            DataManager.LoadDataIntoInstance(this, name);
            ScreenManager.Camera.BackgroundRectangle = BackgroundRectangle;
        }

        public virtual void SetUpKeys()
        {
            ScreenManager.InputManager.KeyInputs.Clear();
        }

        public virtual void LoadScreenContent(ContentManager content)
        {
            foreach (Button b in Buttons)
            {
                b.LoadContent(content);
            }
            foreach (UIBlock ui in UIBlocks)
            {
                ui.LoadContent(content);
            }
        }

        public virtual void UnloadContent()
        {
            Background = null;
            Font = null;
            Buttons = null;
            Images = null;
            Texts = null;
            Title = null;
            UIBlocks = null;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (BackgroundRectangle.Width != 0 && BackgroundRectangle.Height != 0)
            {
                spriteBatch.Draw(Background, BackgroundRectangle, Color.White);
            }
            else
            {
                spriteBatch.Draw(Background, new Rectangle(0, 0, ScreenManager.Viewport.Width, ScreenManager.Viewport.Height), Color.White);
            }

            foreach (Text t in Texts)
            {
                t.Draw(spriteBatch);
            }

            foreach (Image i in Images)
            {
                i.Draw(spriteBatch);
            }

            foreach (Button b in Buttons)
            {
                b.Draw(spriteBatch);
            }

            foreach (UIBlock ui in UIBlocks)
            {
                ui.Draw(spriteBatch);
            }

            if (Title != null)
            {
                Title.Draw(spriteBatch);
            }
        }

        public virtual void InitializeStartingUI(ContentManager content)
        {
            ;
        }

        public virtual void DrawUI(SpriteBatch spriteBatch)
        {
            ;
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (Button b in Buttons)
            {
                b.Update(ScreenManager.InputManager.Mouse.MouseClickedPosition);
            }

            foreach (UIBlock ui in UIBlocks)
            {
                ui.Update(ScreenManager.InputManager.Mouse.MouseClickedPosition);
            }
            
            foreach (Button b in ButtonsToRemove)
            {
                Buttons.Remove(b);
            }

            ButtonsToRemove.Clear();
        }

        public void RemoveButton(Button b)
        {
            ButtonsToRemove.Add(b);
        }

        public void AddButton(Button b)
        {
            if (!Buttons.Contains(b))
            {
                Buttons.Add(b);
            }
        }
    }
}
