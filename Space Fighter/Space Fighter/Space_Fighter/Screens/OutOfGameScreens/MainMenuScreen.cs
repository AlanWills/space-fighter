using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace Space_Fighter
{
    public class MainMenuScreen : Screen
    {
        public MainMenuScreen(ScreenManager sm, DataManager dataManager, string name)
            : base(sm, dataManager, name)
        {
        }

        public override void LoadScreenContent(ContentManager content)
        {
            Font = content.Load<SpriteFont>(FontAsset);
            InitializeStartingUI(content);

            base.LoadScreenContent(content);
        }

        public override void InitializeStartingUI(ContentManager content)
        {
            Vector2 playButtonPos = new Vector2(ScreenManager.Viewport.Width / 2, ScreenManager.Viewport.Height / 4);
            Button playButton = new Button("Play", playButtonPos, "Textures\\UI\\ButtonRegular", "Textures\\UI\\ButtonPressed", Font);
            playButton.ClickEvent += new EventHandler(playButton_ClickEvent);
            Buttons.Add(playButton);

            string text = "Space Fighter";
            Vector2 textPos = new Vector2((ScreenManager.Viewport.Width - Font.MeasureString(text).X) / 2, Font.LineSpacing);
            Title = new Text(text, textPos, Font);
        }

        #region Button Click Events

        void playButton_ClickEvent(object sender, EventArgs e)
        {
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new TravelToWorldScreen(ScreenManager, DataManager, "TravelToWorldScreen"));
        }

        #endregion
    }
}
