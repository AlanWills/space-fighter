﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class GameOverScreen : Screen
    {
        public GameOverScreen(ScreenManager screenManager, DataManager dataManager, string name)
            : base(screenManager, dataManager, name)
        {
        }

        public override void LoadScreenContent(ContentManager content)
        {
            Font = content.Load<SpriteFont>(FontAsset);
            InitializeStartingUI(content);

            base.LoadScreenContent(content);
        }

        public override void InitializeStartingUI(ContentManager content)
        {
            string text = "Game Over";
            Vector2 textPos = new Vector2(ScreenManager.Viewport.Width / 2 - Font.MeasureString(text).X / 2, ScreenManager.Viewport.Height / 2);
            Title = new Text(text, textPos, Font);

            Button b = new Button("Main Menu", new Vector2(textPos.X + Font.MeasureString(text).X / 2, textPos.Y + 3 * Font.LineSpacing + 10),
                "Textures/UI/ButtonRegular", "Textures/UI/ButtonPressed", Font);
            b.ClickEvent += new EventHandler(backToMainMenu_Event);
            Buttons.Add(b);
        }

        #region Button Click Events

        private void backToMainMenu_Event(object sender, EventArgs e)
        {
            ScreenManager.RemoveScreen(this);
            ScreenManager.AddScreen(new MainMenuScreen(ScreenManager, DataManager, "MainMenuScreen"));
        }

        #endregion
    }
}
