﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public enum TurretFireType
    {
        Auto,
        Manual
    }

    public class Turret : GameplayObject
    {
        public List<Bullet> Bullets
        {
            get;
            private set;
        }

        private List<Bullet> BulletsToRemove
        {
            get;
            set;
        }

        public GameplayObject Parent
        {
            get;
            private set;
        }

        public float TargetRotationOffset
        {
            get;
            set;
        }

        public Vector2 Offset
        {
            get;
            private set;
        }

        public Texture2D BulletTexture
        {
            get;
            set;
        }

        public float TurretTimer
        {
            get;
            set;
        }

        private float OffsetRotation
        {
            get;
            set;
        }

        public int TurretDamage
        {
            get;
            set;
        }

        public bool HasTarget
        {
            get;
            set;
        }

        public string Name
        {
            get;
            private set;
        }

        public TechnologyLevel TechLevel
        {
            get;
            set;
        }

        public int Cost
        {
            get;
            set;
        }

        public Turret(Vector2 p, Screen parentScreen, string name, GameplayObject parent, Vector2 offset)
            : base(p, parentScreen)
        {
            Parent = parent;
            Name = name;
            Offset = new Vector2(offset.Y, offset.X);
            OffsetRotation = (float)(Math.PI / 2 + Math.Atan2(Offset.Y, Offset.X));
            TargetRotationOffset = 0;

            Bullets = new List<Bullet>();
            BulletsToRemove = new List<Bullet>();

            DataManager dm = Game1.dataManager;
            dm.LoadDataIntoInstance(this, Name);
        }

        #region Draw Methods

        public override void Draw(SpriteBatch spriteBatch)
        {
            DrawBullets(spriteBatch);

            base.Draw(spriteBatch);
        }

        private void DrawBullets(SpriteBatch spriteBatch)
        {
            foreach (Bullet b in Bullets)
            {
                b.Draw(spriteBatch);
            }
        }

        #endregion

        #region Update Methods

        public override void Update(GameTime gameTime)
        {
            Rotation = TargetRotationOffset;

            Vector2 offsetVec = new Vector2(
                (float)Math.Cos(Parent.Rotation + OffsetRotation), 
                (float)Math.Sin(Parent.Rotation + OffsetRotation));
            offsetVec = Vector2.Normalize(offsetVec);

            Position = Parent.Position + offsetVec * Offset.Length();

            if (TurretTimer > 0)
            {
                TurretTimer -= 0.01f;
            }

            CheckBulletTimer();
            UpdateBullets(gameTime);
            RemoveBullets();
            IsVisible = Parent.IsVisible;
        }

        private void UpdateBullets(GameTime gameTime)
        {
            foreach (Bullet b in Bullets)
            {
                b.Update(gameTime);
            }
        }

        public void CheckBulletTimer()
        {
            if (TurretTimer > 0)
            {
                TurretTimer -= 0.01f;
            }
            else
            {
                if (Parent.FireType == TurretFireType.Auto && HasTarget)
                {
                    FireBullet();
                }
            }
        }

        public void FireBullet()
        {
            Bullet b = new Bullet(Position, ParentScreen, BulletTexture, this);
            Bullets.Add(b);
            TurretTimer = 0.2f;
        }

        public void RemoveBullet(Bullet b)
        {
            BulletsToRemove.Add(b);
        }

        private void RemoveBullets()
        {
            foreach (Bullet b in BulletsToRemove)
            {
                b.Texture = null;
                b.Health = 0;
                Bullets.Remove(b);
            }

            BulletsToRemove.Clear();
        }

        #endregion
    }
}
