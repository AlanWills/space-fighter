﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Bullet : GameplayObject
    {
        private Turret Parent
        {
            get;
            set;
        }

        public Bullet(Vector2 p, Screen parentScreen, Texture2D tex, Turret parent)
            : base(p, parentScreen)
        {
            StartingVelocity = 30;
            Texture = tex;
            Rotation = parent.Rotation + MathHelper.Pi;
            Parent = parent;
            Health = parent.TurretDamage;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            CheckBounds();
            CheckAsteroidCollision();
        }

        private void CheckBounds()
        {
            // See whether the turret the bullet came from is the Player or an Enemy
            Ship p = Parent.Parent as Ship;

            // The bullet came from the Player
            if (p != null && p.GameScreen != null)
            {
                foreach (Enemy e in p.GameScreen.Enemies)
                {
                    if (e.Bounds.Intersects(this.Bounds))
                    {
                        e.Damage(Health);
                        this.Die();
                        break;
                    }
                }
            }
            // The bullet came from an Enemy
            else
            {
                if (GameScreen.ScreenManager.Session.Player.PlayerShip.Bounds.Intersects(this.Bounds))
                {
                    GameScreen.ScreenManager.Session.Player.PlayerShip.Damage(Parent.TurretDamage);
                    this.Die();
                }
            }

            if (!Bounds.Intersects(ParentScreen.BackgroundRectangle))
            {
                Die();
            }
        }

        private void CheckAsteroidCollision()
        {
            if (GameScreen != null)
            {
                ParticleSystem p = GameScreen.ParticleSystem;

                foreach (Asteroid a in p.Particles)
                {
                    if (a.CheckCollision(this))
                    {
                        a.Damage(Health);
                        this.Die();
                        break;
                    }
                }
            }
        }

        public override void Die()
        {
            Parent.RemoveBullet(this);
        }
    }
}
