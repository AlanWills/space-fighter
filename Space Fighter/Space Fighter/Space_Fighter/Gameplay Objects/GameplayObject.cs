using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Space_Fighter
{
    public class GameplayObject
    {
        public Vector2 Position
        {
            get;
            set;
        }

        public float Velocity
        {
            get;
            set;
        }

        public float StartingVelocity
        {
            get;
            protected set;
        }

        public float Acceleration
        {
            get;
            set;
        }

        public float Rotation
        {
            get;
            set;
        }

        public Texture2D Texture
        {
            get;
            set;
        }

        public Rectangle Bounds
        {
            get;
            set;
        }

        public bool IsVisible
        {
            get;
            protected set;
        }

        public int Health
        {
            get;
            set;
        }

        public TurretFireType FireType
        {
            get;
            set;
        }

        public Screen ParentScreen
        {
            get;
            set;
        }

        public GameScreen GameScreen
        {
            get { return ParentScreen as GameScreen; }
        }

        public GameplayObject(Vector2 p, Screen parentScreen)
        {
            Position = p;
            ParentScreen = parentScreen;
        }

        public virtual void LoadContent(ContentManager content)
        {
            Bounds = new Rectangle((int)(Position.X - Texture.Width / 2), (int)(Position.Y - Texture.Height / 2), Texture.Width, Texture.Height);
        }

        public virtual void Update(GameTime gameTime)
        {
            IsVisible = ParentScreen.ScreenManager.Camera.IsVisible(this);
            Velocity = StartingVelocity + Acceleration * (float)gameTime.ElapsedGameTime.Milliseconds;
            Vector2 vel = new Vector2((float)-Math.Sin(Rotation), (float)Math.Cos(Rotation)) * Velocity;
            Position += vel;
            UpdateBounds();
            CheckAlive();
        }

        private void UpdateBounds()
        {
            Bounds = new Rectangle((int)(Position.X - Texture.Width / 2), (int)(Position.Y - Texture.Height / 2), Texture.Width, Texture.Height);
        }

        protected virtual void CheckAlive()
        {
            ;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (IsVisible)
            {
                spriteBatch.Draw(
                    Texture,
                    Position,
                    null,
                    Color.White,
                    Rotation,
                    new Vector2(Texture.Width / 2, Texture.Height / 2),
                    1,
                    SpriteEffects.None,
                    0);
            }
        }

        public virtual void Damage(int damage)
        {
            Health -= damage;
        }

        public virtual void Die()
        {
            ;
        }
    }
}
