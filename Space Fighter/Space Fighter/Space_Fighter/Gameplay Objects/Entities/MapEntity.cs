﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class MapEntity : GameplayObject
    {
        public float RotationDelta
        {
            get;
            protected set;
        }

        public string TextureAsset
        {
            get;
            private set;
        }

        public int Size
        {
            get;
            private set;
        }

        float textureFloat;

        protected EventHandler InteractEvent;

        public MapEntity(Vector2 p, Screen parentScreen, string ta, float f)
            : base(p, parentScreen)
        {
            TextureAsset = ta;
            textureFloat = f;
        }

        public override void LoadContent(ContentManager content)
        {
            Texture = content.Load<Texture2D>(TextureAsset);
            SetupBounds();
        }

        private void SetupBounds()
        {
            Bounds = new Rectangle((int)Position.X, (int)Position.Y, (int)((100 * textureFloat) * Texture.Width / 100), (int)((100 * textureFloat) * Texture.Height / 100));
            Size = Bounds.Width;
            Health = Size;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (IsVisible)
            {
                spriteBatch.Draw(
                    Texture,
                    Bounds,
                    null,
                    Color.White,
                    Rotation,
                    new Vector2(Bounds.Width / 2, Bounds.Height / 2),
                    SpriteEffects.None,
                    0);
            }
        }

        public override void Update(GameTime gameTime)
        {
            IsVisible = ParentScreen.ScreenManager.Camera.IsVisible(this);
            Position += new Vector2(Velocity, 0);
            // Rotation += RotationDelta;
            UpdateBounds();
        }

        private void UpdateBounds()
        {
            Bounds = new Rectangle((int)Position.X, (int)Position.Y, Bounds.Width, Bounds.Height);
        }

        public bool CheckCollision(GameplayObject gpo)
        {
            if (gpo.Bounds.Intersects(Bounds))
            {
                return true;
            }

            return false;
        }

        public override void Die()
        {
        }

        public virtual void OnInteract()
        {
            if (InteractEvent != null)
                InteractEvent(this, EventArgs.Empty);
        }
    }
}
