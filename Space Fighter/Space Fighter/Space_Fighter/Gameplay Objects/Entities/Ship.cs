using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Space_Fighter
{
    public class Ship : GameplayObject
    {
        public List<Turret> Turrets
        {
            get;
            set;
        }

        public List<string> TurretNames
        {
            get;
            set;
        }

        public List<Vector2> TurretOffsets
        {
            get;
            set;
        }

        public bool InertialDampeners
        {
            get;
            private set;
        }

        public bool AutoFireTurrets
        {
            get;
            set;
        }

        public string ShieldsName
        {
            get;
            set;
        }

        public Shield Shields
        {
            get;
            set;
        }

        public int Cost
        {
            get;
            set;
        }

        public bool ShieldsActivated
        {
            get { return Shields.Health > 0; }
        }

        public string Name
        {
            get;
            set;
        }

        public Ship(Vector2 p, Screen parentScreen, string name)
            : base(p, parentScreen)
        {
            Name = name;
            InertialDampeners = true;
            Turrets = new List<Turret>();
            TurretOffsets = new List<Vector2>();
            TurretNames = new List<string>();
            FireType = TurretFireType.Manual;
            AutoFireTurrets = false;
        }

        public override void LoadContent(ContentManager content)
        {
            DataManager dm = Game1.dataManager;
            dm.LoadDataIntoInstance(this, Name);
            SetUpTurrets();
            SetUpShields();

            base.LoadContent(content);
        }

        private void SetUpTurrets()
        {
            foreach (string s in TurretNames)
            {
                AddTurret(Position, s, this);
            }

            TurretNames.Clear();
        }

        private void SetUpShields()
        {
            Shields = new Shield(Position, ParentScreen, ShieldsName, this);
            Game1.dataManager.LoadDataIntoInstance(Shields, ShieldsName);
            Shields.Initialize();
        }

        public void AddTurret(Vector2 p, string name, Ship par)
        {
            if (TurretOffsets.Count > 0)
            {
                Turret t = new Turret(p, ParentScreen, name, par, TurretOffsets[0]);
                Turrets.Add(t);

                TurretOffsets.RemoveAt(0);
            }
        }

        public override void Update(GameTime gameTime)
        {
            CheckAsteroidsCollision();
            if (InertialDampeners)
            {
                Acceleration *= 0.93f;
            }

            MathHelper.Clamp(Acceleration, -0.1f, 0.1f);
            base.Update(gameTime);

            foreach (Turret t in Turrets)
            {
                t.Update(gameTime);
                UpdateTarget(t);
            }

            Shields.Update(gameTime);

            if (Health <= 0)
            {
                ParentScreen.ScreenManager.AddScreen(new GameOverScreen(ParentScreen.ScreenManager, ParentScreen.DataManager, "GameOverScreen"));
                ParentScreen.ScreenManager.RemoveScreen(ParentScreen);
            }
        }

        private void CheckAsteroidsCollision()
        {
            foreach (Asteroid a in GameScreen.ParticleSystem.Particles)
            {
                if (a.CheckCollision(this))
                {
                    if (a.IsAsteroid)
                    {
                        a.Die();
                        Damage(a.Size);
                        Acceleration *= -1;
                    }
                    else
                    {
                        a.Die();
                        ParentScreen.ScreenManager.Session.Money += 30;
                    }
                }
            }
        }

        private void UpdateTarget(Turret t)
        {
            // Implement some sort of firing arc restriction here too.
            if (FireType == TurretFireType.Auto)
            {
                float targetOffset = 0;
                if (GameScreen != null)
                {
                    if (GameScreen.Enemies.Count > 0)
                    {
                        GameplayObject target = null;
                        Vector2 diff = t.Position - GameScreen.Enemies.First().Position;
                        float dist = diff.Length();

                        if (dist < 500)
                        {
                            target = GameScreen.Enemies[0];
                        }

                        Vector2 newEnemyPos;
                        float newDist;

                        for (int i = 1; i < GameScreen.Enemies.Count; i++)
                        {
                            newEnemyPos = GameScreen.Enemies[i].Position;
                            diff = t.Position - newEnemyPos;
                            newDist = diff.Length();

                            if (newDist < dist && newDist < 500)
                            {
                                target = GameScreen.Enemies[i];
                                dist = newDist;
                            }
                        }

                        if (target != null)
                        {
                            Vector2 targetDiff = -target.Position + Position;
                            targetOffset = (float)(Math.Atan2(targetDiff.Y, targetDiff.X) - MathHelper.PiOver2);

                            t.TargetRotationOffset = targetOffset;
                            t.HasTarget = true;
                        }
                        else
                        {
                            t.HasTarget = false;
                            t.TargetRotationOffset = Rotation;
                        }
                    }
                    else
                    {
                        t.TargetRotationOffset = Rotation;
                        t.HasTarget = false;
                    }
                }
            }
            else
            {
                t.Rotation = Rotation + t.TargetRotationOffset;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            foreach (Turret t in Turrets)
            {
                t.Draw(spriteBatch);
            }

            if (ShieldsActivated)
                Shields.Draw(spriteBatch);
        }

        public override void Damage(int damage)
        {
            if (Shields.Health >= damage && Shields.Health > (int)(Shields.MaxHealth / 20))
            {
                Shields.Health -= damage;
            }
            else
            {
                // Add this for a different style of damaging
                /*int currHealth = Shields.Health;
                Shields.Health = 0;

                base.Damage(damage - Shields.Health);
                */

                // With this method, if the shields cannot stop all the damage, they stop none of it and the hull is damaged instead.
                base.Damage(damage);
            }
        }
    }
}
