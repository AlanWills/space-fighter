﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Beacon : MapEntity
    {
        public Text HelpText
        {
            get;
            private set;
        }

        public SpriteFont Font
        {
            get;
            set;
        }

        Screen s;
        ScreenManager sm;

        public Beacon(Vector2 p, Screen parentScreen, string ta, Screen s, ScreenManager sm, float f = 1)
            : base(p, parentScreen, ta, f)
        {
            Velocity = 0;
            Rotation = 0;
            RotationDelta = 0;
            InteractEvent += new EventHandler(travelToWorld_Event);

            this.s = s;
            this.sm = sm;
        }

        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);

            HelpText = new Text("Press R to interact with beacon", Position + new Vector2(0, 30), Font);
        }

        public void DrawText(SpriteBatch spriteBatch)
        {
            HelpText.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            IsVisible = ParentScreen.ScreenManager.Camera.IsVisible(this);
        }

        public void CheckBeaconInteraction(Player p)
        {
            Vector2 diff = p.PlayerShip.Position - Position;

            if (diff.Length() < 200)
                OnInteract();
        }

        private void travelToWorld_Event(object sender, EventArgs e)
        {
            sm.RemoveScreen(s);
            sm.AddScreen(new PlanetDescriptionScreen(ParentScreen.ScreenManager, ParentScreen.DataManager, "PlanetDescriptionScreen", ParentScreen.ScreenManager.Session.DestinationSpaceport));
            ParentScreen.ScreenManager.Session.CurrentSpaceport = ParentScreen.ScreenManager.Session.DestinationSpaceport;
            ParentScreen.ScreenManager.Session.DestinationSpaceport = null;
        }
    }
}
