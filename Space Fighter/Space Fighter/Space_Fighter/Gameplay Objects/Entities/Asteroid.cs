﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Asteroid : MapEntity
    {
        public string OreTextureAsset
        {
            get;
            private set;
        }

        public Texture2D OreTexture
        {
            get;
            set;
        }

        public bool IsAsteroid
        {
            get;
            private set;
        }

        public Asteroid(Vector2 p, Screen parentScreen, string texass, string oreTexAss, float velocity, float rotation, float rotationDelta, float size)
            : base(p, parentScreen, texass, size)
        {
            Velocity = 5 * velocity;
            Rotation = rotation;
            RotationDelta = rotationDelta;
            OreTextureAsset = oreTexAss;
            IsAsteroid = true;
        }

        public override void LoadContent(ContentManager content)
        {
            OreTexture = content.Load<Texture2D>(OreTextureAsset);

            base.LoadContent(content);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Health <= 0 && IsAsteroid)
            {
                GenerateOre();
            }
        }

        private void GenerateOre()
        {
            Texture = OreTexture;
            IsAsteroid = false;
        }

        public override void Die()
        {
            if (GameScreen != null)
            {
                GameScreen.ParticleSystem.ParticlesToRemove.Add(this);
            }
        }
    }
}
