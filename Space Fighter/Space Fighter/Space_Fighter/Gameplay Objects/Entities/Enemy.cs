﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Enemy : GameplayObject
    {
        public List<Turret> Turrets
        {
            get;
            set;
        }

        public List<string> TurretNames
        {
            get;
            set;
        }

        public List<Vector2> TurretOffsets
        {
            get;
            set;
        }

        public bool InertialDampeners
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public string ShieldsName
        {
            get;
            set;
        }

        public int Cost
        {
            get;
            set;
        }

        public Seek Seek
        {
            get;
            private set;
        }

        public bool ShieldsActivated
        {
            get { return false; }
        }

        public Enemy(Vector2 p, Screen parentScreen, string name)
            : base(p, parentScreen)
        {
            Name = name;
            InertialDampeners = true;
            Turrets = new List<Turret>();
            TurretOffsets = new List<Vector2>();
            TurretNames = new List<string>();
            FireType = TurretFireType.Auto;

            DataManager dm = Game1.dataManager;
            dm.LoadDataIntoInstance(this, name);
        }

        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
        }

        public void SetUpTurrets(ContentManager content)
        {
            for (int i = 0; i < TurretNames.Count; i++)
            {
                if (i < TurretOffsets.Count)
                {
                    AddTurret(Position, TurretNames[i], this, TurretOffsets[i]);
                }
            }
        }

        public void AddTurret(Vector2 p, string name, GameplayObject par, Vector2 turretOffset)
        {
            Turret t = new Turret(p, par.ParentScreen, name, par, turretOffset);
            Turrets.Add(t);
        }

        public override void Update(GameTime gameTime)
        {
            if (InertialDampeners)
            {
                Velocity *= 0.90f;
            }

            if (Health > 0 && Seek == null)
            {
                Seek = new Seek(GameScreen.Player.PlayerShip, this);
            }

            Seek.Do();

            // For some reason, clamp does not work - look into it.
            // MathHelper.Clamp(Acceleration, -0.1f, 0.1f);

            if (Acceleration < -0.1f)
                Acceleration = -0.1f;
            if (Acceleration > 0.1f)
                Acceleration = 0.1f;
            base.Update(gameTime);

            foreach (Turret t in Turrets)
            {
                t.Update(gameTime);
                UpdateTarget(t);
            }
        }

        protected override void CheckAlive()
        {
            if (Health <= 0)
            {
                Die();
                ParentScreen.ScreenManager.Session.Money += 200;
            }
        }

        public override void Die()
        {
            if (GameScreen != null)
            {
                GameScreen.EnemiesToRemove.Add(this);
            }
            
            base.Die();
        }

        private void UpdateTarget(Turret t)
        {
            Vector2 playerPos = GameScreen.Player.PlayerShip.Position;
            Vector2 diff = new Vector2(t.Position.X - playerPos.X, t.Position.Y - playerPos.Y);

            float dist = diff.Length();

            if (dist < 500)
            {
                float targetOffset = (float)(Math.Atan2(diff.Y, diff.X) - MathHelper.PiOver2);
                t.TargetRotationOffset = targetOffset;
                t.HasTarget = true;
            }
            else
            {
                t.TargetRotationOffset = 0;
                t.HasTarget = false;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            foreach (Turret t in Turrets)
            {
                t.Draw(spriteBatch);
            }
        }
    }
}
