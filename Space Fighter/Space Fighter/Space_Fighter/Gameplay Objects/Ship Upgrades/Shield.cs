﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Shield
    {
        public Texture2D Texture
        {
            get;
            set;
        }

        public Texture2D TextureAsset
        {
            get;
            set;
        }

        public GameplayObject Parent
        {
            get;
            set;
        }

        public int MaxHealth
        {
            get;
            set;
        }

        public int Health
        {
            get;
            set;
        }

        public TimeSpan RechargeRate
        {
            get;
            set;
        }

        public TechnologyLevel TechLevel
        {
            get;
            set;
        }

        public int Cost
        {
            get;
            set;
        }

        public string Name
        {
            get;
            private set;
        }

        private TimeSpan currentCharge;

        public Shield(Vector2 pos, Screen parentScreen, string name, GameplayObject parentShip)
        {
            Name = name;
            Parent = parentShip;
            currentCharge = RechargeRate;
        }

        public void Initialize()
        {
            Health = MaxHealth;
        }

        public void Update(GameTime gameTime)
        {
            // Recharge shields
            currentCharge = currentCharge - gameTime.ElapsedGameTime;

            if (currentCharge.Milliseconds < 0 && Health < MaxHealth)
            {
                Health++;
                currentCharge = RechargeRate;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            float factor = 2;

            if (Health > (int)(MaxHealth / 5))
            {
                spriteBatch.Draw(
                    Texture,
                    new Rectangle(
                        (int)(Parent.Position.X - factor * Parent.Bounds.Width / 2),
                        (int)(Parent.Position.Y - factor * Parent.Bounds.Height / 2),
                        (int)factor * Parent.Bounds.Width,
                        (int)factor * Parent.Bounds.Height),
                    Color.White);
            }
        }
    }
}
