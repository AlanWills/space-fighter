﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class KeyboardKeyInput
    {
        public List<Keys> Keys
        {
            get;
            private set;
        }

        public bool AllowContinualPress
        {
            get;
            private set;
        }

        public EventHandler WhenKeyDownEvent;

        public KeyboardKeyInput(bool continual)
        {
            Keys = new List<Keys>();
            AllowContinualPress = continual;
        }

        public KeyboardKeyInput(List<Keys> hotkeys, bool continual)
        {
            Keys = hotkeys;
            AllowContinualPress = continual;
        }

        public virtual void OnPress()
        {
            if (WhenKeyDownEvent != null)
            {
                WhenKeyDownEvent(this, EventArgs.Empty);
            }
        }

        public void Update(KeyboardState currState, KeyboardState prevState)
        {
            foreach (Keys k in Keys)
            {
                if (currState.IsKeyDown(k))
                {
                    if (AllowContinualPress)
                    {
                        OnPress();
                    }
                    else if (prevState.IsKeyUp(k))
                    {
                        OnPress();
                    }
                }
            }
        }

        public void AddHotKeys(List<Keys> HotKeys)
        {
            foreach (Keys k in HotKeys)
            {
                Keys.Add(k);
            }
        }
    }
}
