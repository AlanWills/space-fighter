using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Space_Fighter
{
    public class InputManager
    {
        public ScreenManager ScreenManager
        {
            get;
            private set;
        }

        public Mouse Mouse
        {
            get;
            private set;
        }

        public KeyboardState LastKeyboardState
        {
            get;
            private set;
        }

        public KeyboardState CurrentKeyboardState
        {
            get;
            private set;
        }

        public Dictionary<string, KeyboardKeyInput> KeyInputs
        {
            get;
            set;
        }

        public InputManager(ScreenManager sm)
        {
            KeyInputs = new Dictionary<string, KeyboardKeyInput>();
            CurrentKeyboardState = Keyboard.GetState();
            Mouse = new Mouse(this);
            ScreenManager = sm;
        }

        public void AddKey(string name, KeyboardKeyInput key)
        {
            if (KeyInputs.ContainsKey(name) == false)
                KeyInputs.Add(name, key);
        }

        public EventHandler KeyPressed;

        public void Update()
        {
            Mouse.Update();

            LastKeyboardState = CurrentKeyboardState;
            CurrentKeyboardState = Keyboard.GetState();

            foreach (KeyValuePair<string, KeyboardKeyInput> k in KeyInputs)
            {
                k.Value.Update(CurrentKeyboardState, LastKeyboardState);
            }
        }
    }
}
