﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Mouse
    {
        public InputManager InputManager
        {
            get;
            private set;
        }

        public MouseState MouseState
        {
            get;
            private set;
        }

        public Vector2 MousePosition
        {
            get;
            private set;
        }

        public Vector2 MouseClickedPosition
        {
            get;
            private set;
        }

        public Mouse(InputManager im)
        {
            InputManager = im;
            MouseState = GetState();
        }

        public void Update()
        {
            MouseState = GetState();
            MousePosition = new Vector2(MouseState.X, MouseState.Y);

            if (MouseState.LeftButton == ButtonState.Pressed)
            {
                MouseClickedPosition = new Vector2(MouseState.X, MouseState.Y);
            }
            else
            {
                MouseClickedPosition = new Vector2(-1, -1);
            }
        }

        public MouseState GetState()
        {
            return Microsoft.Xna.Framework.Input.Mouse.GetState();
        }
    }
}
