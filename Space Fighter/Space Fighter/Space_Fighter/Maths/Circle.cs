using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Space_Fighter
{
    public class Circle
    {
        public Vector2 Centre
        {
            get;
            private set;
        }

        public float Radius
        {
            get;
            set;
        }

        public Texture2D Texture
        {
            get;
            private set;
        }

        public string TextureAsset
        {
            get;
            private set;
        }

        public Circle(Vector2 p, string ta)
        {
            Centre = p;
            TextureAsset = ta;
        }

        public void LoadContent(ContentManager content)
        {
            Texture = content.Load<Texture2D>(TextureAsset);
            Radius = Texture.Width;
            Centre -= new Vector2(Texture.Width / 2, Texture.Height / 2);
        }

        public void Update(GameTime gameTime)
        {
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Centre, Color.White);
        }
    }
}
