using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Space_Fighter
{
    public class MathUtils
    {
        public static bool PointInRect(Vector2 p, Rectangle r)
        {
            if (p.X >= r.Left 
                && p.X <= r.Right 
                && p.Y >= r.Top 
                && p.Y <= r.Bottom)
            {
                return true;
            }

            return false;
        }
    }
}
