﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public abstract class Behaviour
    {
        public abstract void Do();
    }
}
