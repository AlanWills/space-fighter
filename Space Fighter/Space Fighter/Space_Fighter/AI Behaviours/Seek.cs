﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Seek
    {
        private Ship Target
        {
            get;
            set;
        }

        private Enemy Enemy
        {
            get;
            set;
        }

        public Seek(Ship player, Enemy enemy)
        {
            Target = player;
            Enemy = enemy;
        }

        public void Do()
        {
            Vector2 diff = Target.Position - Enemy.Position;
            float rotation = (float)(Math.Atan2(diff.Y, diff.X) + MathHelper.PiOver2);

            Enemy.Rotation = rotation;

            if (diff.Length() > 200)
            {
                Enemy.Acceleration -= 0.002f;
            }
            else
            {
                Enemy.Acceleration += 0.002f;
            }
        }
    }
}
