﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class DataManager
    {
        public static Dictionary<string, MapData> MapData
        {
            get;
            private set;
        }

        public static Dictionary<string, SpaceportData> WorldsData
        {
            get;
            private set;
        }

        public static Dictionary<string, ShipData> ShipData
        {
            get;
            private set;
        }

        public static Dictionary<string, TurretData> TurretData
        {
            get;
            private set;
        }

        public static Dictionary<string, ShieldData> ShieldData
        {
            get;
            private set;
        }

        public static Dictionary<string, CommodityData> CommodityData
        {
            get;
            private set;
        }

        public static Dictionary<string, QuestData> QuestsData
        {
            get;
            private set;
        }

        public void InitializeDataManager()
        {
            MapData = new Dictionary<string, MapData>();
            WorldsData = new Dictionary<string, SpaceportData>();
            ShipData = new Dictionary<string, ShipData>();
            TurretData = new Dictionary<string, TurretData>();
            ShieldData = new Dictionary<string, ShieldData>();
            CommodityData = new Dictionary<string, CommodityData>();
            QuestsData = new Dictionary<string, QuestData>();
        }

        #region Get and Set for Dictionaries

        public MapData GetMapData(string name)
        {
            if (MapData.ContainsKey(name))
                return MapData[name];
            else
                return null;
        }

        public SpaceportData GetSpaceportData(string name)
        {
            if (WorldsData.ContainsKey(name))
                return WorldsData[name];
            else if (MapData.ContainsKey(name))
                return (SpaceportData)MapData[name];
            else
                return null;
        }

        private void SetUpMapData()
        {
            MapData.Add("MainMenuScreen", new MainMenuScreenData());
            MapData.Add("GameOverScreen", new GameOverScreenData());
            MapData.Add("TravelToWorldScreen", new TravelToWorldScreenData());
            MapData.Add("PlanetDescriptionScreen", new PlanetDescriptionScreenData());

            MapData.Add("DeepSpace", new DeepSpaceData());
            MapData.Add("SevenPlanets", new SevenPlanetsData());
            MapData.Add("Supernova", new SupernovaData());
            MapData.Add("FrozenWastes", new FrozenWastesData());
            MapData.Add("AsteroidBelt", new AsteroidBeltData());
            MapData.Add("BurningSky", new BurningSkyData());
            MapData.Add("Nebula", new NebulaData());
            MapData.Add("RedPlanet", new RedPlanetData());

            MapData.Add("Terra Spaceport", new TerraSpaceportScreenData());
            MapData.Add("Anthea Spaceport", new AntheaSpaceportScreenData());
            MapData.Add("Telegra Spaceport", new TelegraSpaceportScreenData());
            MapData.Add("Elektrin Spaceport", new ElektrinSpaceportScreenData());
            MapData.Add("Freiza Spaceport", new FreizaSpaceportScreenData());
            MapData.Add("Zion Spaceport", new ZionSpaceportScreenData());
            MapData.Add("Pyron Spaceport", new PyronSpaceportScreenData());
            MapData.Add("Ranbron Spaceport", new RanbronSpaceportScreenData());

            MapData.Add("Defeat Alien Invader", new AlienBossQuestData());
            MapData.Add("Pirate Invasion", new PirateInvasionData());

            WorldsData.Add("Terra", new TerraSpaceportScreenData());
            WorldsData.Add("Anthea", new AntheaSpaceportScreenData());
            WorldsData.Add("Telegra", new TelegraSpaceportScreenData());
            WorldsData.Add("Elektrin", new ElektrinSpaceportScreenData());
            WorldsData.Add("Freiza", new FreizaSpaceportScreenData());
            WorldsData.Add("Zion", new ZionSpaceportScreenData());
            WorldsData.Add("Pyron", new PyronSpaceportScreenData());
            WorldsData.Add("Ranbron", new RanbronSpaceportScreenData());
        }

        public ShipData GetShipData(string name)
        {
            if (ShipData.ContainsKey(name))
                return ShipData[name];
            else
                return null;
        }

        private void SetUpShipData()
        {
            ShipData.Add("Dragonfly", new DragonflyData());
            ShipData.Add("Wasp", new WaspData());
            ShipData.Add("Hornet", new HornetData());
            ShipData.Add("Austria", new AustriaData());
            ShipData.Add("Belgium", new BelgiumData());
            ShipData.Add("Bulgaria", new BulgariaData());
            ShipData.Add("Pirate Tusker", new PirateTuskerData());
            ShipData.Add("Pirate Hunter", new PirateHunterData());
        }

        public TurretData GetTurretData(string name)
        {
            if (TurretData.ContainsKey(name))
                return TurretData[name];
            else
                return null;
        }

        private void SetUpTurretData()
        {
            TurretData.Add("zsu14", new zsu14Data());
            TurretData.Add("42mm Chain", new _42mmChainData());
            TurretData.Add("Heavy Blaster", new HeavyBlasterData());
        }

        public ShieldData GetShieldData(string name)
        {
            if (ShieldData.ContainsKey(name))
                return ShieldData[name];
            else return null;
        }

        private void SetUpShieldData()
        {
            ShieldData.Add("Level 1", new Level1ShieldData());
            ShieldData.Add("Level 2", new Level2ShieldData());
            ShieldData.Add("Level 3", new Level3ShieldData());
        }

        public CommodityData GetCommodityData(string name)
        {
            if (CommodityData.ContainsKey(name))
                return CommodityData[name];
            else return null;
        }

        private void SetUpCommodityData()
        {
            CommodityData.Add("Food", new FoodCommodityData());
            CommodityData.Add("Fuel", new FuelCommodityData());
            CommodityData.Add("Medicine", new MedicineCommodityData());
            CommodityData.Add("Narcotics", new NarcoticsCommodityData());
            CommodityData.Add("Electronics", new ElectronicsCommodityData());
        }

        public QuestData GetQuestData(string name)
        {
            if (QuestsData.ContainsKey(name))
                return QuestsData[name];
            else return null;
        }

        private void SetUpQuestsData()
        {
            QuestsData.Add("Defeat Alien Invader", new AlienBossQuestData());
            QuestsData.Add("Pirate Invasion", new PirateInvasionData());
        }

        #endregion

        #region Load the Content

        public void LoadAllGameContent(ContentManager content)
        {
            SetUpQuestsData();
            SetUpCommodityData();
            SetUpTurretData();
            SetUpShieldData();
            SetUpShipData();
            SetUpMapData();
            LoadQuestsData(content);
            LoadCommodityData(content);
            LoadTurretContent(content);
            LoadShieldContent(content);
            LoadShipContent(content);
            LoadMapContent(content);
        }

        private void LoadQuestsData(ContentManager content)
        {
            foreach (var qEntry in QuestsData)
            {
                qEntry.Value.LoadContent(content);
            }
        }

        private void LoadCommodityData(ContentManager content)
        {
            foreach (var cEntry in CommodityData)
            {
                cEntry.Value.LoadContent(content);
            }
        }

        private void LoadTurretContent(ContentManager content)
        {
            foreach (var tEntry in TurretData)
            {
                tEntry.Value.LoadContent(content);
            }
        }

        private void LoadShieldContent(ContentManager content)
        {
            foreach (var sEntry in ShieldData)
            {
                sEntry.Value.LoadContent(content);
            }
        }

        private void LoadShipContent(ContentManager content)
        {
            foreach (var sEntry in ShipData)
            {
                sEntry.Value.LoadContent(content);
            }
        }

        private void LoadMapContent(ContentManager content)
        {
            foreach (var mEntry in MapData)
            {
                mEntry.Value.LoadContent(content);
            }

            foreach (var wEntry in WorldsData)
            {
                wEntry.Value.LoadContent(content);
            }
        }

        #endregion

        #region For Cloning Data

        public void LoadDataIntoInstance(Screen s, string key)
        {
            MapData m = GetMapData(key);
            s.BackgroundRectangle = m.BackgroundRectangle;
            s.FontAsset = m.FontAsset;
            s.Background = m.Texture;

            GameScreen g = s as GameScreen;
            SpaceData spaceData = m as SpaceData;
            if (g != null && spaceData != null)
            {
                g.AsteroidNumber = spaceData.AsteroidNumber;
                g.BeaconLocation = spaceData.BeaconLocation;
                g.BeaconTextureAsset = spaceData.BeaconTextureAsset;
                g.EnemyNames = spaceData.EnemyNames;
            }

            QuestScreen q = s as QuestScreen;
            QuestData questData = m as QuestData;
            if (q != null && questData != null)
            {
                q.QuestDescription = questData.QuestDescription;
                q.RewardMoney = questData.RewardMoney;
                q.RewardCommodities = questData.RewardCommodities;
                q.RewardTurrets = questData.RewardTurrets;
                q.RewardShields = questData.RewardShields;
            }

            SpaceportScreen sp = s as SpaceportScreen;
            SpaceportData spaceportData = m as SpaceportData;
            if (sp != null)
            {
                sp.TechLevel = spaceportData.TechLevel;
                sp.WorldName = spaceportData.WorldName;
            }
        }

        public void LoadDataIntoInstance(Ship p, string key)
        {
            ShipData sd = GetShipData(key);

            p.Name = key;
            p.Texture = sd.Texture;
            p.TurretOffsets = sd.TurretOffsets;

            if (p.Turrets.Count == 0)
            {
                p.TurretNames = sd.StartingTurretNames;
            }

            p.Health = sd.Health;

            if (p.ShieldsName == "" || p.ShieldsName == null)
            {
                p.ShieldsName = sd.ShieldsName;
            }

            p.Cost = sd.Cost;
        }

        public void LoadDataIntoInstance(Enemy e, string key)
        {
            ShipData sd = GetShipData(key);

            e.Texture = sd.Texture;
            e.TurretOffsets = sd.TurretOffsets;
            e.TurretNames = sd.StartingTurretNames;
            e.Health = sd.Health;
            e.ShieldsName = sd.ShieldsName;
            e.Cost = sd.Cost;
        }

        public void LoadDataIntoInstance(Shield s, string key)
        {
            if (key != "")
            {
                ShieldData sd = GetShieldData(key);

                s.Texture = sd.Texture;
                s.RechargeRate = sd.RechargeRate;
                s.MaxHealth = sd.Health;
                s.TechLevel = sd.TechLevel;
                s.Cost = sd.Cost;
            }
        }

        public void LoadDataIntoInstance(Turret t, string key)
        {
            TurretData td = GetTurretData(key);

            t.BulletTexture = td.BulletTexture;
            t.Texture = td.Texture;
            t.TurretTimer = td.TurretTimer;
            t.TurretDamage = td.TurretDamage;
            t.TechLevel = td.TechLevel;
            t.Cost = td.Cost;
        }

        public void LoadDataIntoInstance(Commodity c, string key)
        {
            CommodityData cd = GetCommodityData(key);

            c.Name = cd.Name;
            c.Cost = cd.Cost;
            c.TechLevel = cd.TechLevel;
            c.Legal = cd.Legal;
        }

        #endregion
    }
}
