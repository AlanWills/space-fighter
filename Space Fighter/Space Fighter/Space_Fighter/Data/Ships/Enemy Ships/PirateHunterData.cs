﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class PirateHunterData : ShipData
    {
        public PirateHunterData()
        {
            TextureAsset = "Textures\\Sprites\\Enemy Ships\\pir-fighter2";
            TurretOffsets = new List<Vector2>()
            {
                new Vector2(11, 2),
                new Vector2(-11, 2)
            };
            StartingTurretNames = new List<string>()
            {
                "zsu14",
                "zsu14"
            };
            Health = 150;
            ShieldsName = "";
            Cost = 700;
            TechLevel = TechnologyLevel.Primitive;
        }
    }
}
