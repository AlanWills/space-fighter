﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class PirateTuskerData : ShipData
    {
        public PirateTuskerData()
        {
            TextureAsset = "Textures\\Sprites\\Enemy Ships\\pir-fighter1";
            TurretOffsets = new List<Vector2>()
            {
                new Vector2(11, 2),
                new Vector2(-11, 2)
            };
            StartingTurretNames = new List<string>()
            {
                "zsu14",
                "zsu14"
            };
            Health = 100;
            ShieldsName = "";
            Cost = 500;
            TechLevel = TechnologyLevel.Primitive;
        }
    }
}
