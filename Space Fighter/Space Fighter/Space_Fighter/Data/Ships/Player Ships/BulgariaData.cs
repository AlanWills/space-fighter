﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class BulgariaData : ShipData
    {
        public BulgariaData()
        {
            TextureAsset = "Textures\\Sprites\\Player Ships\\Bulgaria";
            TurretOffsets = new List<Vector2>()
            {
            };
            StartingTurretNames = new List<string>()
            {
                "zsu14",
                "zsu14",
                "zsu14"
            };
            Health = 400;
            ShieldsName = "";
            TechLevel = TechnologyLevel.Developed;
            Cost = 4000;
        }
    }
}
