﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class AustriaData : ShipData
    {
        public AustriaData()
        {
            TextureAsset = "Textures\\Sprites\\Player Ships\\Austria";
            TurretOffsets = new List<Vector2>()
            {
            };
            StartingTurretNames = new List<string>()
            {
                "zsu14",
                "zsu14"
            };
            Health = 500;
            ShieldsName = "";
            TechLevel = TechnologyLevel.Developed;
            Cost = 2000;
        }
    }
}
