﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class DragonflyData : ShipData
    {
        public DragonflyData()
        {
            TextureAsset = "Textures\\Sprites\\Player Ships\\Dragonfly";
            TurretOffsets = new List<Vector2>()
            {
                new Vector2(13, 0),
                new Vector2(-13, 0),
                new Vector2(0, -20),
                new Vector2(0, 20)
            };
            StartingTurretNames = new List<string>()
            {
                "zsu14",
                "zsu14"
            };
            Health = 250;
            ShieldsName = "";
            TechLevel = TechnologyLevel.Primitive;
            Cost = 1000;
        }
    }
}
