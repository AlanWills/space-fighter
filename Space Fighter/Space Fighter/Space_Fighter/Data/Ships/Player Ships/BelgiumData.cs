﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class BelgiumData : ShipData
    {
        public BelgiumData()
        {
            TextureAsset = "Textures\\Sprites\\Player Ships\\Belgium";
            TurretOffsets = new List<Vector2>()
            {
            };
            StartingTurretNames = new List<string>()
            {
                "zsu14",
                "zsu14"
            };
            Health = 600;
            ShieldsName = "";
            TechLevel = TechnologyLevel.Developed;
            Cost = 2500;
        }
    }
}
