﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class HornetData : ShipData
    {
        public HornetData()
        {
            TextureAsset = "Textures\\Sprites\\Player Ships\\Hornet";
            TurretOffsets = new List<Vector2>()
            {
                /*new Vector2(34, 10),
                new Vector2(-34, 10),
                new Vector2(63, -5),
                new Vector2(-63, -5),
                new Vector2(63, -33),
                new Vector2(-63, -33),
                new Vector2(33, 68),
                new Vector2(-33, 68),
                new Vector2(35, 163),
                new Vector2(-35, 163)*/
            };
            StartingTurretNames = new List<string>()
            {
                "zsu14",
                "zsu14"
            };
            Health = 2000;
            ShieldsName = "";
            TechLevel = TechnologyLevel.Advanced;
            Cost = 7000;
        }
    }
}
