﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class ShipData
    {
        public string TextureAsset
        {
            get;
            protected set;
        }

        public Texture2D Texture
        {
            get;
            set;
        }

        public List<Vector2> TurretOffsets
        {
            get;
            protected set;
        }

        public List<string> StartingTurretNames
        {
            get;
            protected set;
        }

        public int Health
        {
            get;
            protected set;
        }

        public string ShieldsName
        {
            get;
            protected set;
        }

        public int Cost
        {
            get;
            protected set;
        }

        public TechnologyLevel TechLevel
        {
            get;
            set;
        }

        public void LoadContent(ContentManager content)
        {
            Texture = content.Load<Texture2D>(TextureAsset);
        }
    }
}
