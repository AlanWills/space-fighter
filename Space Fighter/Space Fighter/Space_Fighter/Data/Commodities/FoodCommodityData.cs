﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class FoodCommodityData : CommodityData
    {
        public FoodCommodityData()
        {
            Name = "Food";
            Cost = 10;
            TechLevel = TechnologyLevel.Primitive;
            Legal = true;
            ThumbnailAsset = "Textures\\Sprites\\Commodities\\Food";
        }
    }
}
