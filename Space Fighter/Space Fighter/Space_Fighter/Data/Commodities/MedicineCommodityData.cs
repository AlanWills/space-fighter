﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class MedicineCommodityData : CommodityData
    {
        public MedicineCommodityData()
        {
            Name = "Medicine";
            Cost = 500;
            TechLevel = TechnologyLevel.Developed;
            Legal = true;
            ThumbnailAsset = "Textures\\Sprites\\Commodities\\Medicine";
        }
    }
}
