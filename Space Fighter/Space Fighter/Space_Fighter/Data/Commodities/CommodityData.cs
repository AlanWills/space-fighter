﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class CommodityData
    {
        public string Name
        {
            get;
            set;
        }

        public int Cost
        {
            get;
            set;
        }

        public TechnologyLevel TechLevel
        {
            get;
            set;
        }

        public TechnologyLevel TargetTechLevel
        {
            get;
            set;
        }

        public bool Legal
        {
            get;
            set;
        }

        public Texture2D Thumbnail
        {
            get;
            set;
        }

        public string ThumbnailAsset
        {
            get;
            set;
        }

        public virtual void LoadContent(ContentManager content)
        {
            Thumbnail = content.Load<Texture2D>(ThumbnailAsset);
        }
    }
}
