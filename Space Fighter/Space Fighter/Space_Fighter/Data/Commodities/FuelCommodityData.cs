﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class FuelCommodityData : CommodityData
    {
        public FuelCommodityData()
        {
            Name = "Fuel";
            Cost = 200;
            TechLevel = TechnologyLevel.Developed;
            TargetTechLevel = TechnologyLevel.Primitive;
            Legal = true;
            ThumbnailAsset = "Textures\\Sprites\\Commodities\\Fuel";
        }
    }
}
