﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class NarcoticsCommodityData : CommodityData
    {
        public NarcoticsCommodityData()
        {
            Name = "Narcotics";
            Cost = 1500;
            TechLevel = TechnologyLevel.Developed;
            Legal = false;
            ThumbnailAsset = "Textures\\Sprites\\Commodities\\Narcotics";
        }
    }
}
