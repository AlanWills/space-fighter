﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class ElectronicsCommodityData : CommodityData
    {
        public ElectronicsCommodityData()
        {
            Name = "Electronics";
            Cost = 2000;
            TechLevel = TechnologyLevel.Advanced;
            TargetTechLevel = TechnologyLevel.Developed;
            Legal = true;
            ThumbnailAsset = "Textures\\Sprites\\Commodities\\Electronics";
        }
    }
}
