﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class SevenPlanetsData : SpaceData
    {
        public SevenPlanetsData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 3840, 2048);
            TextureAsset = "Textures\\Backgrounds\\Space\\The Seven";
            FontAsset = "Fonts\\GameScreenFont";
            AsteroidDensity = Space_Fighter.AsteroidDensity.Low;
            EnemyDensity = Space_Fighter.EnemyDensity.Low;
            BeaconLocation = new Vector2(100, 100);
            BeaconTextureAsset = "Textures\\Sprites\\Map Elements\\Beacon";
            EnemyNames = new List<string>()
            {
                "Pirate Tusker",
                "Pirate Tusker"
            };
        }
    }
}
