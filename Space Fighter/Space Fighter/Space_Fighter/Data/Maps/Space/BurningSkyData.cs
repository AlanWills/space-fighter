﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class BurningSkyData : SpaceData
    {
        public BurningSkyData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 3840, 2048);
            TextureAsset = "Textures\\Backgrounds\\Space\\Burning Sky";
            FontAsset = "Fonts\\GameScreenFont";
            AsteroidDensity = Space_Fighter.AsteroidDensity.Low;
            EnemyDensity = Space_Fighter.EnemyDensity.High;
            BeaconLocation = new Vector2(3750, 1000);
            BeaconTextureAsset = "Textures\\Sprites\\Map Elements\\Beacon";
            EnemyNames = new List<string>()
            {
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker"
            };
        }
    }
}
