﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class RedPlanetData : SpaceData
    {
        public RedPlanetData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 3840, 2048);
            TextureAsset = "Textures\\Backgrounds\\Space\\The Red Planet";
            FontAsset = "Fonts\\GameScreenFont";
            AsteroidDensity = Space_Fighter.AsteroidDensity.Medium;
            EnemyDensity = Space_Fighter.EnemyDensity.Medium;
            BeaconLocation = new Vector2(500, 500);
            BeaconTextureAsset = "Textures\\Sprites\\Map Elements\\Beacon";
            EnemyNames = new List<string>()
            {
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker"
            };
        }
    }
}
