﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public enum AsteroidDensity
    {
        Low = 10,
        Medium = 20,
        High = 30
    }

    public enum EnemyDensity
    {
        Low = 2,
        Medium = 4,
        High = 6
    }

    public class SpaceData : MapData
    {
        public AsteroidDensity AsteroidDensity
        {
            get;
            set;
        }

        public EnemyDensity EnemyDensity
        {
            get;
            set;
        }

        public int AsteroidNumber
        {
            get;
            set;
        }

        public int EnemyNumber
        {
            get;
            set;
        }

        public List<string> EnemyNames
        {
            get;
            set;
        }

        public override void LoadContent(Microsoft.Xna.Framework.Content.ContentManager content)
        {
            base.LoadContent(content);

            AsteroidNumber = (int)AsteroidDensity;
            EnemyNumber = (int)EnemyDensity;
        }
    }
}
