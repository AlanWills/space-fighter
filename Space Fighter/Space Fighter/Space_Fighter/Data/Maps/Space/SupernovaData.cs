﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class SupernovaData : SpaceData
    {
        public SupernovaData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 5760, 3072);
            TextureAsset = "Textures\\Backgrounds\\Space\\Supernova";
            FontAsset = "Fonts\\GameScreenFont";
            AsteroidDensity = Space_Fighter.AsteroidDensity.High;
            EnemyDensity = Space_Fighter.EnemyDensity.Low;
            BeaconLocation = new Vector2(300, 3000);
            BeaconTextureAsset = "Textures\\Sprites\\Map Elements\\Beacon";
            EnemyNames = new List<string>()
            {
                "Pirate Tusker",
                "Pirate Tusker"
            };
        }
    }
}
