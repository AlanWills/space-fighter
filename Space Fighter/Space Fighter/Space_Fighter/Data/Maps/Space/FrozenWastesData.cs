﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class FrozenWastesData : SpaceData
    {
        public FrozenWastesData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 3840, 2048);
            TextureAsset = "Textures\\Backgrounds\\Space\\Frozen Wastes";
            FontAsset = "Fonts\\GameScreenFont";
            AsteroidDensity = Space_Fighter.AsteroidDensity.High;
            EnemyDensity = Space_Fighter.EnemyDensity.High;
            BeaconLocation = new Vector2(3750, 1950);
            BeaconTextureAsset = "Textures\\Sprites\\Map Elements\\Beacon";
            EnemyNames = new List<string>()
            {
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker"
            };
        }
    }
}
