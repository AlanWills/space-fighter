﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class NebulaData : SpaceData
    {
        public NebulaData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 5760, 3072);
            TextureAsset = "Textures\\Backgrounds\\Space\\Nebula";
            FontAsset = "Fonts\\GameScreenFont";
            AsteroidDensity = Space_Fighter.AsteroidDensity.Low;
            EnemyDensity = Space_Fighter.EnemyDensity.High;
            BeaconLocation = new Vector2(4000, 200);
            BeaconTextureAsset = "Textures\\Sprites\\Map Elements\\Beacon";
            EnemyNames = new List<string>()
            {
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker"
            };
        }
    }
}
