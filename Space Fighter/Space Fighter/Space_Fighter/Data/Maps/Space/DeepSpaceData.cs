﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class DeepSpaceData : SpaceData
    {
        public DeepSpaceData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 3000, 3000);
            TextureAsset = "Textures\\Backgrounds\\Space\\Deep Space";
            FontAsset = "Fonts\\GameScreenFont";
            AsteroidDensity = Space_Fighter.AsteroidDensity.Medium;
            EnemyDensity = Space_Fighter.EnemyDensity.Medium;
            BeaconLocation = new Vector2(2500, 2700);
            BeaconTextureAsset = "Textures\\Sprites\\Map Elements\\Beacon";
            EnemyNames = new List<string>()
            {
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker"
            };
        }
    }
}
