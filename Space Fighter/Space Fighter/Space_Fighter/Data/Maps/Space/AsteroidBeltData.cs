﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class AsteroidBeltData : SpaceData
    {
        public AsteroidBeltData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 1920, 1024);
            TextureAsset = "Textures\\Backgrounds\\Space\\Asteroid Belt";
            FontAsset = "Fonts\\GameScreenFont";
            AsteroidDensity = Space_Fighter.AsteroidDensity.High;
            EnemyDensity = Space_Fighter.EnemyDensity.High;
            BeaconLocation = new Vector2(100, 100);
            BeaconTextureAsset = "Textures\\Sprites\\Map Elements\\Beacon";
            EnemyNames = new List<string>()
            {
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker"
            };
        }
    }
}
