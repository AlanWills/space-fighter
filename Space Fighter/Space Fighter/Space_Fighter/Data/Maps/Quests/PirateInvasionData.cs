﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class PirateInvasionData : QuestData
    {
        public PirateInvasionData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 5760, 3072);
            TextureAsset = "Textures\\Backgrounds\\Space\\The Seven";
            FontAsset = "Fonts\\GameScreenFont";
            AsteroidDensity = Space_Fighter.AsteroidDensity.Low;
            EnemyDensity = Space_Fighter.EnemyDensity.High;
            BeaconLocation = new Vector2(100, 100);
            BeaconTextureAsset = "Textures\\Sprites\\Map Elements\\Beacon";
            // First name HAS to be the name of the boss!
            EnemyNames = new List<string>()
            {
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Hunter",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker",
                "Pirate Tusker"
            };
            QuestName = "Pirate Invasion";
            QuestDescription = "Pirates have invaded the galaxy.\nIt will require an extremely well-equipped vessel to take on their sheer numbers.";
            RewardMoney = 30000;
            RewardCommodities = new List<string>()
            {

            };
            RewardTurrets = new List<string>()
            {

            };
            RewardShields = new List<string>()
            {

            };
        }
    }
}
