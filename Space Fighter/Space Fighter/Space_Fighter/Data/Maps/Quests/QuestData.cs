﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class QuestData : SpaceData
    {
        public string QuestName
        {
            get;
            set;
        }

        public string QuestDescription
        {
            get;
            set;
        }

        public int RewardMoney
        {
            get;
            set;
        }

        public List<string> RewardCommodities
        {
            get;
            set;
        }

        public List<string> RewardTurrets
        {
            get;
            set;
        }

        public List<string> RewardShields
        {
            get;
            set;
        }
    }
}
