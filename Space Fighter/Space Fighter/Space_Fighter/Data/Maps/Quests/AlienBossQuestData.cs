﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class AlienBossQuestData : QuestData
    {
        public AlienBossQuestData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 3840, 2048);
            TextureAsset = "Textures\\Backgrounds\\Space\\Asteroid Belt";
            FontAsset = "Fonts\\GameScreenFont";
            AsteroidDensity = Space_Fighter.AsteroidDensity.High;
            EnemyDensity = Space_Fighter.EnemyDensity.Low;
            BeaconLocation = new Vector2(100, 100);
            BeaconTextureAsset = "Textures\\Sprites\\Map Elements\\Beacon";
            // First name HAS to be the name of the boss!
            EnemyNames = new List<string>()
            {
                "Pirate Hunter"
            };
            QuestName = "Defeat Alien Invader";
            QuestDescription = "An Alien race from an unknown area of the galaxy has invaded our quadrant.\nThe ship is harbouring in a nearby asteroid field.\nBeware!\nIt is an incredibly advanced vessel with superior firepower\nand the asteroids will only add to the danger";
            RewardMoney = 4000;
            RewardCommodities = new List<string>()
            {
                "Food"
            };
            RewardTurrets = new List<string>()
            {
                "42mm Chain",
                "Heavy Blaster"
            };
            RewardShields = new List<string>()
            {
                "Level 1"
            };
        }
    }
}
