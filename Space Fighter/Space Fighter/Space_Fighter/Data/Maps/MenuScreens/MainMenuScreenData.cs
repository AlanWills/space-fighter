﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class MainMenuScreenData : MapData
    {
        public MainMenuScreenData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 1920, 1080);
            TextureAsset = "Textures\\Backgrounds\\Screens\\HighResMainMenu";
            FontAsset = "Fonts\\GameScreenFont";
        }
    }
}
