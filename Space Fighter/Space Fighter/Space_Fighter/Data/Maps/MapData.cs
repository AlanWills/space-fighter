﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class MapData
    {
        public Rectangle BackgroundRectangle
        {
            get;
            set;
        }

        public string FontAsset
        {
            get;
            set;
        }

        public string TextureAsset
        {
            get;
            set;
        }

        public Texture2D Texture
        {
            get;
            set;
        }

        public Vector2 BeaconLocation
        {
            get;
            set;
        }

        public string BeaconTextureAsset
        {
            get;
            set;
        }

        public virtual void LoadContent(ContentManager content)
        {
            Texture = content.Load<Texture2D>(TextureAsset);
        }
    }
}
