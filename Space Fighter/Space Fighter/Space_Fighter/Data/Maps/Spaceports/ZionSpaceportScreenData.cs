﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class ZionSpaceportScreenData : SpaceportData
    {
        public ZionSpaceportScreenData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 1920, 1080);
            TextureAsset = "Textures\\Backgrounds\\Planets\\Zion\\ZionSpaceport";
            FontAsset = "Fonts\\GameScreenFont";
            ThumbnailTextureAsset = "Textures\\Backgrounds\\Planets\\Zion\\ZionThumbnail";
            WorldName = "Zion";
            TechLevel = TechnologyLevel.Advanced;
            MapPosition = new Vector2(1500, 100);
            SpaceRouteName = "SevenPlanets";
            DescriptionText = new List<string>()
            {
                "Zion",
                "The self-proclaimed centre of civilization in the galaxy",
                "Their technology is some of the best around",
                "But they have little sympathy for those who would disrupt their peace"
            };
            QuestsAvailable = new List<string>()
            {
                "Pirate Invasion"
            };
        }
    }
}
