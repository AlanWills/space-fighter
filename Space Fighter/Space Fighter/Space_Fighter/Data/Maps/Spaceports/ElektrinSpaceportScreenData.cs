﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class ElektrinSpaceportScreenData : SpaceportData
    {
        public ElektrinSpaceportScreenData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 1920, 1080);
            TextureAsset = "Textures\\Backgrounds\\Planets\\Elektrin\\ElektrinSpaceport";
            FontAsset = "Fonts\\GameScreenFont";
            ThumbnailTextureAsset = "Textures\\Backgrounds\\Planets\\Elektrin\\ElektrinThumbnail";
            WorldName = "Elektrin";
            TechLevel = TechnologyLevel.Developed;
            MapPosition = new Vector2(1500, 700);
            SpaceRouteName = "DeepSpace";
            DescriptionText = new List<string>()
            {
                "Elektrin",
                "A rapidly developing planet",
                "Over 90% of the planet is covered by one vast, sprawling city",
                "Expect much from this planet in the future"
            };
        }
    }
}
