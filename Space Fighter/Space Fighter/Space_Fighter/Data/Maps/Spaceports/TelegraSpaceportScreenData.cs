﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class TelegraSpaceportScreenData : SpaceportData
    {
        public TelegraSpaceportScreenData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 1920, 1080);
            TextureAsset = "Textures\\Backgrounds\\Planets\\Telegra\\TelegraSpaceport";
            FontAsset = "Fonts\\GameScreenFont";
            ThumbnailTextureAsset = "Textures\\Backgrounds\\Planets\\Telegra\\TelegraThumbnail";
            WorldName = "Telegra";
            TechLevel = TechnologyLevel.Primitive;
            MapPosition = new Vector2(1000, 800);
            SpaceRouteName = "FrozenWastes";
            DescriptionText = new List<string>()
            {
                "Telegra",
                "A planet surrounded by the Frozen Wastes",
                "The people here have very little and basic necessities are always needed",
                "Expect a warm welcome, but little else"
            };
        }
    }
}
