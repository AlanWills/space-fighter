﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class AntheaSpaceportScreenData : SpaceportData
    {
        public AntheaSpaceportScreenData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 1920, 1080);
            TextureAsset = "Textures\\Backgrounds\\Planets\\Anthea\\AntheaSpaceport";
            FontAsset = "Fonts\\GameScreenFont";
            ThumbnailTextureAsset = "Textures\\Backgrounds\\Planets\\Anthea\\AntheaThumbnail";
            WorldName = "Anthea";
            TechLevel = TechnologyLevel.Advanced;
            MapPosition = new Vector2(500, 300);
            SpaceRouteName = "Supernova";
            DescriptionText = new List<string>()
            {
                "Anthea",
                "A highly advanced, but secluded civilization",
                "A society of philosophers and mathematicians who care little for the troubles of the galaxy",
                "DO NOT start an argument with an Anthean!"
            };
        }
    }
}
