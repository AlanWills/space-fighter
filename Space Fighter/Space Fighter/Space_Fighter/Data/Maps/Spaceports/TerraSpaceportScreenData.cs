﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class TerraSpaceportScreenData : SpaceportData
    {
        public TerraSpaceportScreenData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 1920, 1080);
            TextureAsset = "Textures\\Backgrounds\\Planets\\Terra\\TerranSpaceport";
            FontAsset = "Fonts\\GameScreenFont";
            ThumbnailTextureAsset = "Textures\\Backgrounds\\Planets\\Terra\\TerraThumbnail";
            WorldName = "Terra";
            TechLevel = TechnologyLevel.Developed;
            MapPosition = new Vector2(100, 100);
            SpaceRouteName = "AsteroidBelt";
            DescriptionText = new List<string>()
            {
                "Terra",
                "Original homeworld of the humans who have now expanded out amongst the stars",
                "Here you will find a reasonable range of equipment and goods",
                "For a price..."
            };
            QuestsAvailable = new List<string>()
            {
                "Defeat Alien Invader"
            };
        }
    }
}
