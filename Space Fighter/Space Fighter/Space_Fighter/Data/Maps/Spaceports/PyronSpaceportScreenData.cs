﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class PyronSpaceportScreenData : SpaceportData
    {
        public PyronSpaceportScreenData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 1920, 1080);
            TextureAsset = "Textures\\Backgrounds\\Planets\\Pyron\\PyronSpaceport";
            FontAsset = "Fonts\\GameScreenFont";
            ThumbnailTextureAsset = "Textures\\Backgrounds\\Planets\\Pyron\\PyronThumbnail";
            WorldName = "Pyron";
            TechLevel = TechnologyLevel.Primitive;
            MapPosition = new Vector2(900, 200);
            SpaceRouteName = "BurningSky";
            DescriptionText = new List<string>()
            {
                "Pyron",
                "Planet of eternal fire",
                "Inhabitants consist of desperate savages and criminals",
                "Despite it's heat, DO NOT expect a warm welcome here"
            };
        }
    }
}
