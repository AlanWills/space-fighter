﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class RanbronSpaceportScreenData : SpaceportData
    {
        public RanbronSpaceportScreenData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 1920, 1080);
            TextureAsset = "Textures\\Backgrounds\\Planets\\Ranbron\\RanbronSpaceport";
            FontAsset = "Fonts\\GameScreenFont";
            ThumbnailTextureAsset = "Textures\\Backgrounds\\Planets\\Ranbron\\RanbronThumbnail";
            WorldName = "Ranbron";
            TechLevel = TechnologyLevel.Developed;
            MapPosition = new Vector2(700, 800);
            SpaceRouteName = "RedPlanet";
            DescriptionText = new List<string>()
            {
                "Ranbron",
                "Little is known about the history of this planet",
                "They have technology capable of space travel, but seem reluctant to leave",
                "The planet's glow seems unusual..."
            };
        }
    }
}
