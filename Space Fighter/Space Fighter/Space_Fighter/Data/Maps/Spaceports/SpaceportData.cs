﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public enum TechnologyLevel
    {
        Primitive,
        Developed,
        Advanced
    }

    public class SpaceportData : MapData
    {
        public string WorldName
        {
            get;
            protected set;
        }

        public TechnologyLevel TechLevel
        {
            get;
            protected set;
        }

        public string ThumbnailTextureAsset
        {
            get;
            protected set;
        }

        public string SpaceRouteName
        {
            get;
            protected set;
        }

        public Texture2D ThumbnailTexture
        {
            get;
            set;
        }

        public Vector2 MapPosition
        {
            get;
            set;
        }

        public List<string> DescriptionText
        {
            get;
            set;
        }

        public List<string> QuestsAvailable
        {
            get;
            set;
        }

        public override void LoadContent(ContentManager content)
        {
            ThumbnailTexture = content.Load<Texture2D>(ThumbnailTextureAsset);

            base.LoadContent(content);
        }
    }
}
