﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class FreizaSpaceportScreenData : SpaceportData
    {
        public FreizaSpaceportScreenData()
        {
            BackgroundRectangle = new Rectangle(0, 0, 1920, 1080);
            TextureAsset = "Textures\\Backgrounds\\Planets\\Freiza\\FreizaSpaceport";
            FontAsset = "Fonts\\GameScreenFont";
            ThumbnailTextureAsset = "Textures\\Backgrounds\\Planets\\Freiza\\FreizaThumbnail";
            WorldName = "Freiza";
            TechLevel = TechnologyLevel.Primitive;
            MapPosition = new Vector2(300, 950);
            SpaceRouteName = "Nebula";
            DescriptionText = new List<string>()
            {
                "Freiza",
                "A fairly unremarkable planet",
                "It's wildlife is unmatched anywhere in the galaxy and the slow pace of life is highly saught after",
                "Do not expect much to happen here"
            };
        }
    }
}
