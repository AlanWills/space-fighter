﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class TurretData
    {
        public Texture2D BulletTexture
        {
            get;
            private set;
        }

        public string BulletTextureAsset
        {
            get;
            protected set;
        }

        public Texture2D Texture
        {
            get;
            private set;
        }

        public string TextureAsset
        {
            get;
            protected set;
        }

        public float TurretTimer
        {
            get;
            protected set;
        }

        public int TurretDamage
        {
            get;
            protected set;
        }

        public TechnologyLevel TechLevel
        {
            get;
            protected set;
        }

        public int Cost
        {
            get;
            protected set;
        }

        public void LoadContent(ContentManager content)
        {
            BulletTexture = content.Load<Texture2D>(BulletTextureAsset);
            Texture = content.Load<Texture2D>(TextureAsset);
        }
    }
}
