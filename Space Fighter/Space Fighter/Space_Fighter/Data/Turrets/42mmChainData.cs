﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class _42mmChainData : TurretData
    {
        public _42mmChainData()
        {
            TextureAsset = "Textures\\Sprites\\Weapons\\42mm Chain\\42mm_chain_hard00";
            BulletTextureAsset = "Textures\\Sprites\\Weapons\\42mm Chain\\42mm_recoil";
            TurretTimer = 0.05f;
            TurretDamage = 5;
            TechLevel = TechnologyLevel.Developed;
            Cost = 400;
        }
    }
}
