﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class zsu14Data : TurretData
    {
        public zsu14Data()
        {
            TextureAsset = "Textures\\Sprites\\Weapons\\zsu14\\zsu14_turret_base";
            BulletTextureAsset = "Textures\\Sprites\\Weapons\\zsu14\\zsu14_turret_recoil";
            TurretTimer = 0.2f;
            TurretDamage = 3;
            TechLevel = TechnologyLevel.Primitive;
            Cost = 150;
        }
    }
}
