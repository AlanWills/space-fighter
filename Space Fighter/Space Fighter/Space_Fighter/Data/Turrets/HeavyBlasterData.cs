﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class HeavyBlasterData : TurretData
    {
        public HeavyBlasterData()
        {
            TextureAsset = "Textures\\Sprites\\Weapons\\Heavy Blaster\\alien_hvy_blaster";
            BulletTextureAsset = "Textures\\Sprites\\Weapons\\Heavy Blaster\\alien_hvy_blaster_recoil";
            TurretTimer = 0.8f;
            TurretDamage = 30;
            TechLevel = TechnologyLevel.Developed;
            Cost = 800;
        }
    }
}
