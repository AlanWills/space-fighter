﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Level2ShieldData : ShieldData
    {
        public Level2ShieldData()
        {
            TextureAsset = "Textures\\Sprites\\Shields\\Shield2";
            Health = 120;
            RechargeRate = TimeSpan.FromMilliseconds(90);
            TechLevel = TechnologyLevel.Primitive;
            Cost = 300;
        }
    }
}
