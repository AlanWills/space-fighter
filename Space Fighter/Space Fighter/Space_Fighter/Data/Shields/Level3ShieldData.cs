﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Level3ShieldData : ShieldData
    {
        public Level3ShieldData()
        {
            TextureAsset = "Textures\\Sprites\\Shields\\Shield3";
            Health = 200;
            RechargeRate = TimeSpan.FromMilliseconds(180);
            TechLevel = TechnologyLevel.Primitive;
            Cost = 350;
        }
    }
}
