﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Level1ShieldData : ShieldData
    {
        public Level1ShieldData()
        {
            TextureAsset = "Textures\\Sprites\\Shields\\Shield1";
            Health = 100;
            RechargeRate = TimeSpan.FromMilliseconds(100);
            TechLevel = TechnologyLevel.Primitive;
            Cost = 200;
        }
    }
}
