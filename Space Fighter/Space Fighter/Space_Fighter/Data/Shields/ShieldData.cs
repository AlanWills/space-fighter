﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class ShieldData
    {
        public string TextureAsset
        {
            get;
            set;
        }

        public Texture2D Texture
        {
            get;
            set;
        }

        public int Health
        {
            get;
            set;
        }

        public TimeSpan RechargeRate
        {
            get;
            set;
        }

        public TechnologyLevel TechLevel
        {
            get;
            protected set;
        }

        public int Cost
        {
            get;
            protected set;
        }

        public void LoadContent(ContentManager content)
        {
            Texture = content.Load<Texture2D>(TextureAsset);
        }
    }
}
