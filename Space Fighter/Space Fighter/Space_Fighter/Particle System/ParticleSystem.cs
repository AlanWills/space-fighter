﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class ParticleSystem
    {
        public List<Asteroid> Particles
        {
            get;
            private set;
        }

        public List<Asteroid> ParticlesToRemove
        {
            get;
            private set;
        }

        public ScreenManager ScreenManager
        {
            get;
            private set;
        }

        public ParticleSystem(ScreenManager sm)
        {
            Particles = new List<Asteroid>();
            ParticlesToRemove = new List<Asteroid>();
            ScreenManager = sm;
        }

        public void LoadContent(ContentManager content)
        {
            foreach (Asteroid m in Particles)
            {
                m.LoadContent(content);
            }
        }

        public void Update(GameTime gameTime)
        {
            foreach (Asteroid m in Particles)
            {
                m.Update(gameTime);

                if (m.Bounds.Left > ScreenManager.CurrentScreen.BackgroundRectangle.Right)
                {
                    m.Position = new Vector2(ScreenManager.CurrentScreen.BackgroundRectangle.Left, m.Position.Y);
                }
            }

            foreach (Asteroid m in ParticlesToRemove)
            {
                Particles.Remove(m);
            }

            ParticlesToRemove.Clear();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Asteroid m in Particles)
            {
                m.Draw(spriteBatch);
            }
        }

        public void Add(Asteroid m)
        {
            Particles.Add(m);
        }
    }
}
