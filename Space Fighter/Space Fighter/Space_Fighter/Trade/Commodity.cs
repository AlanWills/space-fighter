﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Commodity
    {
        public bool Legal
        {
            get;
            set;
        }

        public Texture2D Thumbnail
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public int Cost
        {
            get;
            set;
        }

        public TechnologyLevel TechLevel
        {
            get;
            set;
        }
    }
}
