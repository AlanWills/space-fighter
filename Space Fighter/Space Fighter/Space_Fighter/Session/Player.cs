﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class Player
    {
        public Ship PlayerShip
        {
            get;
            set;
        }

        public Player()
        {
            PlayerShip = new Ship(Vector2.Zero, null, "Dragonfly");
        }
    }
}
