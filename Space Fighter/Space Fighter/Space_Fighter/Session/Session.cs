﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Space_Fighter
{
    public class ShipEventArgs : EventArgs
    {
        public Ship s;
        public ShipEventArgs(Ship s)
        {
            this.s = s;
        }
    }

    public class Session
    {
        public DataManager DataManager
        {
            get;
            private set;
        }

        public Player Player
        {
            get;
            private set;
        }

        public Ship PlayerShip
        {
            get { return Player.PlayerShip; }
        }

        public List<Commodity> Cargo
        {
            get;
            private set;
        }

        public int Money
        {
            get;
            set;
        }

        public string CurrentSpaceport
        {
            get;
            set;
        }

        public string DestinationSpaceport
        {
            get;
            set;
        }

        public delegate void PurchaseShipEventHandler(object sender, ShipEventArgs sea);
        public event EventHandler PlayerShipPurchased;
        public event EventHandler PlayerTurretPurchased;

        public Session(DataManager dm)
        {
            DataManager = dm;
            DestinationSpaceport = "Terra";
            Player = new Player();
            Cargo = new List<Commodity>();
            Money = 1000;
        }

        public void PurchaseShip(string shipKey)
        {
            Player.PlayerShip.Turrets.Clear();
            DataManager.LoadDataIntoInstance(Player.PlayerShip, shipKey);

            Money -= Player.PlayerShip.Cost;
            if (PlayerShipPurchased != null)
            {
                PlayerShipPurchased(this, EventArgs.Empty);
            }
        }

        public void PurchaseTurret(string turretKey)
        {
            TurretData td = DataManager.GetTurretData(turretKey);
            ShipData sd = DataManager.GetShipData(PlayerShip.Name);

            if (PlayerShip.TurretOffsets.Count > 0 && td.TechLevel <= sd.TechLevel)
            {
                Turret t = new Turret(PlayerShip.Position, PlayerShip.ParentScreen, turretKey, PlayerShip, PlayerShip.TurretOffsets[0]);
                PlayerShip.TurretOffsets.RemoveAt(0);

                Money -= t.Cost;
                PlayerShip.Turrets.Add(t);
                PlayerShip.TurretNames.Add(turretKey);
                if (PlayerTurretPurchased != null)
                {
                    PlayerTurretPurchased(this, EventArgs.Empty);
                }
            }
        }

        public void PurchaseShield(string shieldKey)
        {
            ShieldData s = DataManager.GetShieldData(shieldKey);

            PlayerShip.ShieldsName = shieldKey;
            Money -= s.Cost;
        }

        public bool HasCommodity(string name)
        {
            foreach (Commodity c in Cargo)
            {
                if (c.Name == name)
                    return true;
            }

            return false;
        }

        public void BuyCargo(string name, int buyPrice)
        {
            Commodity c = new Commodity();
            DataManager.LoadDataIntoInstance(c, name);

            Cargo.Add(c);
            Money -= buyPrice;
        }

        public void SellCargo(string name, int sellPrice)
        {
            Commodity c = Cargo.Find(x => x.Name == name);
            Cargo.Remove(c);

            Money += sellPrice;
        }
    }
}
